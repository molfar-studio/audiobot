package com.crossfeed.favdoc;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import java.io.Serializable;

/**
 * Created by hawk on 06.01.2017.
 */
public class AudioItem implements Serializable {
    String link, name, author;
    int list_mark;
    long id, ownerId;
    Drawable art;
    int duration;
    boolean isPlaylist;
    String size;
    int back;
    String picUrl;
    Bitmap pic;
    long lastModified;

    public AudioItem () {
        link = ""; name = "Untitled"; author="Unknown artist"; list_mark=-1; id=-1;ownerId=-1;
        duration = 0; isPlaylist = false; size=""; picUrl="";back=-1; pic = null;
    }

   /* public void setListMark(int mark) {
        list_mark = mark;
    }

    public int getListMark() {
        return list_mark;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getMp3Link() {
        return link;
    }

    public void setMp3Name(String name) {
        this.name = name;
    }

    public String getMp3Name() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public Drawable getArt() {
        return art;
    }

    public void setArt(Drawable art) {
        this.art = art;
    }*/
}
