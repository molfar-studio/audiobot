package com.crossfeed.favdoc;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by hawk on 08.01.2017.
 */
public class SearchFragment extends Fragment implements View.OnClickListener, TextView.OnEditorActionListener {

    //EditText search_et;
    String query;
    ListView main_body;
    ProgressBar progressBar;
    private long last_click_time=0;
    private Context context;
    String user_agent = "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)";
    String search_request = "https://www.google.ru/search?q=";
    AudioHelper audioHelper;
    Searcher searcher;
    BrowserInfo browserInfo;
    RelativeLayout bottom_player;
    //Button search_bt;
    AppCompatActivity act;
    private View view;
    TextView empty_text;
    final int SEARCH_LIST_MARK = 0;
    //BottomPlayer bottomPlayer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        act = (AppCompatActivity) getActivity();

        context = act.getApplicationContext();

        //audioHelper = MainActivity.getAudioHelper();
        browserInfo = new BrowserInfo();
        searcher = new Searcher(browserInfo, context);
        //bottomPlayer = MainActivity.getBottomPlayer();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)  {
        if(view==null) {
            view = inflater.inflate(R.layout.search_fragment, container, false);
            setExternalReferences(view);
            initialize();
        }
        return view;
    }

    public void initialize() {

        //search_bt.setOnClickListener(this);
       // search_et.setOnEditorActionListener(this);

        onStreamPlaylistClickListener clickListener = MainActivity.getClickListener();
        if(clickListener!=null)
            main_body.setOnItemClickListener(clickListener);

        main_body.setEmptyView(empty_text);

    }

    public void setExternalReferences(View view){
       // search_et = (EditText) act.findViewById(search_et);
        //search_bt = (Button) act.findViewById(R.id.search_bt);
        main_body = (ListView) view.findViewById(R.id.playlist_main);
        progressBar = (ProgressBar) view.findViewById(R.id.main_progressbar);
        progressBar.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.blue), PorterDuff.Mode.MULTIPLY);
        //bottom_player = (RelativeLayout) act.findViewById(R.id.bottomPlayer);
        empty_text = (TextView) view.findViewById(R.id.empty_text);
    }



    public void search_for_query() {

        if (!CurrentPlayInfo.isNetworkConnected(context)) {
            Toast.makeText(context,
                    R.string.network_access_error_text, Toast.LENGTH_SHORT).show();
            return;
        }
        if (SystemClock.elapsedRealtime() - last_click_time < 2000)
            return;

        last_click_time = SystemClock.elapsedRealtime();
        //query = search_et.getText().toString();

        if(query.equals(""))
            return;

        AsyncTask<String, Void, ArrayList<AudioItem>> searching = new AsyncTask<String,
                Void, ArrayList<AudioItem>>() {
            @Override
            protected void onPreExecute() {
                main_body.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected ArrayList<AudioItem> doInBackground(String... param) {
                ArrayList<String> urls = searcher.searchLinksByGoogle(param[0], search_request);
                return searcher.getLightScanMp3Links(urls);
            }

            @Override
            protected void onPostExecute(ArrayList<AudioItem> result) {
                PlayListAdapter adapter = new PlayListAdapter(result, context);
                main_body.setAdapter(adapter);
                //bottomPlayer.setSearchListLength(result.size());
                progressBar.setVisibility(View.GONE);
                main_body.setVisibility(View.VISIBLE);
            }
        };
        searching.execute(query);
    }

    @Override
    public void onClick(View view) {
        search_for_query();
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if(actionId == EditorInfo.IME_ACTION_SEARCH) {
            search_for_query();
            return true;
        }
        return false;
    }
}
