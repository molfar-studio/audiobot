package com.crossfeed.favdoc;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import io.gresse.hugo.vumeterlibrary.VuMeterView;

import static com.crossfeed.favdoc.R.id.vumeter;

/**
 * Created by hawk on 06.01.2017.
 */
public class PlayListAdapter extends BaseAdapter {
    ArrayList<AudioItem> songs;
    private Context context;
    private LayoutInflater inflater;
    //int focused_position;
    int focused_list;
    //BottomPlayer bottomPlayer;
    onStreamPlaylistClickListener clickListener;
    ViewHolder viewHolder;
    CurrentPlayInfo playInfo;
    AudioHelper audioHelper;
    private SparseBooleanArray selectedItemsIds;
    public ActionMode actionMode;
    public String playlistName = "";
    public ArrayList<AudioItem> playlist;
    public String lastFmArtUrl;
    public int dividerPos = 0;

    public PlayListAdapter(ArrayList<AudioItem> songs, Context context) {
        this.context = context;
        this.songs = songs;
        //bottomPlayer = MainActivity.getBottomPlayer();
        clickListener = MainActivity.getClickListener();
        audioHelper = clickListener.getAudioHelper();
        playInfo = MainActivity.getPlayInfo();
        selectedItemsIds = new SparseBooleanArray();

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return songs.get(position);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        if (view == null) {
            view = inflater.inflate(R.layout.audio_list_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.song_title = (TextView) view.findViewById(R.id.songTitle);
            viewHolder.buttonHolder = (FrameLayout) view.findViewById(R.id.button_holder);
            viewHolder.song_author = (TextView) view.findViewById(R.id.songAuthor);
            viewHolder.vumeter = (VuMeterView) view.findViewById(vumeter);
            viewHolder.songDuration = (TextView) view.findViewById(R.id.song_durationTv);
            viewHolder.vumeter.stop(true);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        switch (MainActivity.theme_code) {
            case 0:
                viewHolder.song_title.setTextColor(context.getResources().getColor(R.color.black));
                if (dividerPos != 0 && position < dividerPos) {
                    view.setBackgroundResource(R.color.shuffle_grey);
                } else if (selectedItemsIds.get(position))
                    view.setBackgroundColor(0x9934B5E4);
                else
                    view.setBackgroundColor(Color.TRANSPARENT);
                break;
            case 1:
                viewHolder.song_title.setTextColor(context.getResources().getColor(R.color.white));
                if (dividerPos != 0 && position < dividerPos) {
                    view.setBackgroundResource(R.color.ac_gray);
                } else if (selectedItemsIds.get(position))
                    view.setBackgroundColor(0x9934B5E4);
                else
                    view.setBackgroundColor(Color.TRANSPARENT);
                break;
        }


        AudioItem item = (AudioItem) getItem(position);
        ImageView itemPlay = (ImageView) view.findViewById(R.id.play_button_item);
        //itemPlay.setBackgroundResource(item.back);
        //VuMeterView vumeter = (VuMeterView) view.findViewById(R.id.vumeter);

        focused_list = playInfo.ACTIVE_LIST_MARK;
        String active_link = playInfo.activeLink;


        if (item.link.equals(active_link)) {
            audioHelper.setButtonHolder(viewHolder.buttonHolder, itemPlay, viewHolder.vumeter);
            viewHolder.vumeter.setVisibility(View.VISIBLE);
            itemPlay.setVisibility(View.INVISIBLE);

            if (audioHelper.getIsPlaying() || audioHelper.getIsConnecting()) {
                viewHolder.vumeter.resume(true);
            } else if (audioHelper.getIsSongConnected()) {
                viewHolder.vumeter.stop(true);
            }
        } else {
            viewHolder.vumeter.setVisibility(View.INVISIBLE);
            itemPlay.setVisibility(View.VISIBLE);
        }

        viewHolder.song_title.setText(item.name.replace(".mp3", ""));
        viewHolder.song_author.setText(item.author);


        //viewHolder.song_title.setCompoundDrawables(null, null, null, null);
        itemPlay.setBackgroundResource(R.drawable.disc_main1);

        int duration = item.duration;
        if (duration != 0)
            viewHolder.songDuration.setText(duration / 60000 + ((duration / 1000) % 60 < 10 ? ":0" : ":") + (duration / 1000) % 60);
        else
            viewHolder.songDuration.setText("");

        if (lastFmArtUrl != null)
            Picasso.with(context).load(Uri.parse(lastFmArtUrl)).transform(new CircleTransform()).into(itemPlay);
        else if (!item.picUrl.equals("")) {
            Picasso.with(context).load(Uri.parse(item.picUrl)).transform(new CircleTransform()).into(itemPlay);
        } else {
            itemPlay.setImageBitmap(null);
            if (item.isPlaylist) {
            /*viewHolder.song_title.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_playlist_play_black_24dp, 0, 0, 0);*/
                itemPlay.setBackgroundResource(R.drawable.ic_playlist_play_black_24px);
            } else
                itemPlay.setBackgroundResource(R.drawable.disc_main1);
        }


        return view;
    }

    public void remove(AudioItem object) {
        songs.remove(object);
        notifyDataSetChanged();
    }

    public void remove(ArrayList<AudioItem> objects) {
        for (int i = 0; i < objects.size(); i++) {
            songs.remove(objects.get(i));
        }
    }

    public int findMatchPos(String text) {
        int pos = 0;
        for (int i = 0; i < songs.size(); i++) {
            if (songs.get(i).name.toLowerCase().contains(text.toLowerCase())) {
                pos = i;
                break;
            }
        }
        return pos;
    }


    public void toggleSelection(int position) {
        selectView(position, !selectedItemsIds.get(position));
    }

    public void removeSelection() {
        selectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public void setPlaylistDataSet(SharedPreferences prefs) {
        songs = audioHelper.getFavsList(prefs);
        notifyDataSetChanged();
    }

    public void selectView(int position, boolean value) {
        if (value)
            selectedItemsIds.put(position, value);
        else
            selectedItemsIds.delete(position);
        notifyDataSetChanged();
    }

    public void updateActionModeTitle() {
        final int checked = getSelectedCount();
        actionMode.setTitle(checked + context.getResources().getString(R.string.tb_selected));
    }

    public int getSelectedCount() {
        return selectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return selectedItemsIds;
    }

    public static class ViewHolder {
        public TextView song_title;
        public TextView song_author;
        public FrameLayout buttonHolder;
        public VuMeterView vumeter;
        public TextView songDuration;
    }

    public ArrayList<AudioItem> getActivePlaylist() {
        return songs;
    }

    public void setActivePlaylist(ArrayList<AudioItem> list) {
        playlist = list;
    }

}



























