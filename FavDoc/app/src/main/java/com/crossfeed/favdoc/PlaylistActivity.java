package com.crossfeed.favdoc;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import static com.crossfeed.favdoc.AudioHelper.BROADCAST_ACTION;
import static com.crossfeed.favdoc.AudioHelper.ON_BUFFER;
import static com.crossfeed.favdoc.AudioHelper.ON_PAUSE;
import static com.crossfeed.favdoc.AudioHelper.ON_PLAY;
import static com.crossfeed.favdoc.AudioHelper.ON_UNBUFFER;
import static com.crossfeed.favdoc.AudioHelper.PARAM_TASK;


/**
 * Created by ivan on 16.08.17.
 */

public class PlaylistActivity extends AppCompatActivity implements View.OnClickListener,
        AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener, AbsListView.OnScrollListener {

    ListView playlistFavs;
    LinearLayout emptyView;
    BottomPlayer bottomPlayer;
    CurrentPlayInfo playInfo;
    FloatingActionButton fab;

    private ActionMode actionMode;
    AudioHelper audioHelper;
    //String path = Environment.getExternalStorageDirectory().getPath()+"/favdoc";
    String path;
    Context context;
    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    SharedPreferences playlistSize;
    PlayListAdapter adapter;
    Toolbar toolbar;
    SharedPreferences.Editor playlistEdit;
    SharedPreferences playlistPrefs;
    String playlistTitle;
    EditText searchSelect;
    int mLastFirstVisibleItem=0;
    boolean goingUp = false;
    public final static int ON_RELEASE = 8;
    public final static int ON_CONNECTING = 9;

    BroadcastReceiver releaseReceiver;
    boolean hasBottomPlayer = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        switch (MainActivity.theme_code){
            case 0:
                break;
            case 1:
                setTheme(R.style.AppTheme_Dark);
                break;
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist);

        RelativeLayout root = (RelativeLayout)findViewById(R.id.root_layout_playlist);
        switch (MainActivity.theme_code){
            case 0:
                root.setBackgroundColor(getResources().getColor(R.color.white));
                break;
            case 1:
                root.setBackgroundColor(getResources().getColor(R.color.bg_gray));
                break;
        }

        context = getApplicationContext();
        playlistTitle = getIntent().getStringExtra("name");
        playlistSize = context.getSharedPreferences("playListsSize",0);

        playInfo = MainActivity.getPlayInfo();

        if(playInfo.isExternalStorageWritable())
            path = MainActivity.path;

        audioHelper =  MainActivity.getClickListener().getAudioHelper();
        prefs = context.getSharedPreferences(playlistTitle, 0);
        playlistPrefs = context.getSharedPreferences("playLists2",0);
        bottomPlayer = new BottomPlayer(PlaylistActivity.this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        releaseReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int task = intent.getIntExtra(PARAM_TASK, 0);
                switch (task) {
                    case ON_RELEASE:
                        bottomPlayer.setLayoutVisibility(false);
                        playlistFavs.refreshDrawableState();
                        hasBottomPlayer = false;
                        break;

                    case AudioHelper.ON_PREPARED_TASK:
                        if(playInfo.currentSong!=null && !hasBottomPlayer) {
                            bottomPlayer.initialize(playInfo.currentSong, audioHelper);
                            bottomPlayer.setBottomPlayer();
                            bottomPlayer.setPlaybottom();
                            playlistFavs.refreshDrawableState();
                            //hasBottomPlayer = true;
                        }
                        break;
                    case ON_PAUSE:
                    case ON_PLAY:
                    case ON_BUFFER:
                    case ON_UNBUFFER:
                        if (bottomPlayer != null)
                            bottomPlayer.setPlaybottom();
                        break;

                }
            }
        };
        IntentFilter intentFilter = new IntentFilter(BROADCAST_ACTION);
        registerReceiver(releaseReceiver, intentFilter);

        if(playInfo.currentSong!=null) {
            bottomPlayer = new BottomPlayer(PlaylistActivity.this);
            bottomPlayer.initialize(playInfo.currentSong, audioHelper);
            bottomPlayer.setBottomPlayer();
            bottomPlayer.setPlaybottom();
            hasBottomPlayer = true;
        }

        fab = (FloatingActionButton) findViewById(R.id.fab);

    }

    @Override
    protected void onStart() {
        super.onStart();

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(playlistTitle);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_keyboard_backspace_white_24dp));

        playlistFavs = (ListView) findViewById(R.id.playlist_favs);
        searchSelect = (EditText) findViewById(R.id.search_select);
        emptyView = (LinearLayout) findViewById(R.id.empty_favs);
        playlistFavs.setEmptyView(emptyView);

        playlistFavs.setOnItemClickListener(this);
        playlistFavs.setOnItemLongClickListener(this);
        playlistFavs.setOnScrollListener(this);
        fab.setOnClickListener(this);
    }


    @Override
    protected void onResume() {
        super.onResume();

        if(playlistTitle.equals("all")) {
            adapter = new PlayListAdapter(audioHelper.loadMp3s(path), context);
            fab.setVisibility(View.GONE);
        }
        else
            adapter = new PlayListAdapter(audioHelper.getFavsList(prefs), context);

        adapter.playlistName = playlistTitle;
        playlistFavs.setAdapter(adapter);

        edit = playlistSize.edit();
        edit.putInt(playlistTitle, adapter.getCount());
        edit.apply();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(releaseReceiver);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab:
                addMode();
                break;
        }
    }

    void addMode() {
        adapter = new PlayListAdapter(audioHelper.loadMp3s(path), context);
        playlistFavs.setAdapter(adapter);
        adapter.playlistName = playlistTitle;
        //fab.setVisibility(View.GONE);

        playlistFavs.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        startActionMode(
                new ActionModeCallback(playlistFavs, this, prefs, fab, searchSelect, playlistTitle, 2));
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long id) {

        if (adapter.actionMode != null) {
            return false;
        }

        playlistFavs.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        playlistFavs.setItemChecked(pos, true);
        //fab.setVisibility(View.GONE);

        startActionMode(
                new ActionModeCallback(playlistFavs, this, prefs, fab, null, playlistTitle, 1));
        adapter.toggleSelection(pos);

        adapter.actionMode.setTitle("1" + getResources().getString(R.string.tb_selected));

        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        AudioItem song = (AudioItem)parent.getItemAtPosition(position);
        String link = song.link;

        if(adapter.actionMode!=null) {
            adapter.toggleSelection(position);
            adapter.updateActionModeTitle();
        }

        else if (link.equals(playInfo.activeLink) &&
                song.list_mark == playInfo.ACTIVE_LIST_MARK) {

            if (audioHelper.getIsPlaying()) {
                audioHelper.stopMp3();
            }
            else if(audioHelper.getIsSongConnected()){
                audioHelper.playMp3();
            }
            else
                audioHelper.releaseMp3();

            if (bottomPlayer != null)
                bottomPlayer.setPlaybottom();
        }
        else {
            /*if (!CurrentPlayInfo.isNetworkConnected(context)) {
                Toast.makeText(context,
                        R.string.network_access_error_text, Toast.LENGTH_SHORT).show();
                return;
            }*/
            audioHelper.setFocusParams(position, view);
            audioHelper.setParent(parent);

           /* if(audioHelper!=null) {
                audioHelper.releaseMp3();
                audioHelper.setActivePlaylist(((PlayListAdapter)parent.getAdapter()).getActivePlaylist());
                audioHelper.setActivePlaylistIndex(((PlayListAdapter)parent.getAdapter()).getActivePlaylist().indexOf(parent.getItemAtPosition(position)));
                audioHelper.setConnectionMp3((AudioItem) parent.getItemAtPosition(position),
                        ((PlayListAdapter)parent.getAdapter()).getActivePlaylist(), view);
            }*/

            if(audioHelper!=null) {
                ArrayList<AudioItem> list = ((PlayListAdapter)parent.getAdapter()).getActivePlaylist();

                audioHelper.releaseMp3();
                audioHelper.setActivePlaylist(list);
                audioHelper.setActivePlaylistIndex(list.indexOf(parent.getItemAtPosition(position)));
                audioHelper.setConnectionMp3((AudioItem) parent.getItemAtPosition(position), list, view);
            }

           /* if(playInfo.currentSong!=null && !hasBottomPlayer) {
                bottomPlayer = new BottomPlayer(this);
                bottomPlayer.initialize(playInfo.currentSong, audioHelper);
                bottomPlayer.setBottomPlayer();
                bottomPlayer.unsetProgress();
                bottomPlayer.setPlaylistPlaybottom();
            }*/
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_playlist, menu);



       /* MenuItem searchMenuItem = menu.findItem(R.id.search_btn);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) searchMenuItem.getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Search");*/

        //MenuItemCompat.setOnActionExpandListener(searchMenuItem, this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.delete_playlist:
                final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setTitle(getResources().getString(R.string.popup_delete)+ playlistTitle+"?");
                dialog.setPositiveButton(getResources().getString(R.string.popup_btn_delete), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface nameDialog, int which) {
                        playlistEdit = playlistPrefs.edit();
                        playlistEdit.putInt(playlistTitle, -1);
                        playlistEdit.apply();
                        nameDialog.cancel();
                        finish();
                    }
                });
                dialog.setNegativeButton(getResources().getString(R.string.popup_btn_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface nameDialog, int which) {
                        nameDialog.cancel();
                    }
                });
                dialog.show();
                break;
            case R.id.add2playlist:
                addMode();
                break;
           /* case R.id.sortWay:
                break;*/

            case R.id.infoPlaylist:

                Dialog  infoDialog = new Dialog(this);
                infoDialog.setTitle(getResources().getString(R.string.menu_playlist_info));
                //infoDialog.setIcon(R.drawable.ic_playlist_add_check_black_24dp);
                infoDialog.setContentView(R.layout.info_playlist_popup);

                TextView full_name = (TextView) infoDialog.findViewById(R.id.full_name_value);
                TextView size = (TextView) infoDialog.findViewById(R.id.size_value);
                TextView title = (TextView) infoDialog.findViewById(R.id.title_tv);

                title.setText(playlistTitle);
                full_name.setText(playlistTitle);
                size.setText("0 items");



                infoDialog.show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int scrollState) {

        int btn_initPosY=fab.getScrollY();
        /*if (scrollState == SCROLL_STATE_TOUCH_SCROLL) {
            fab.animate().cancel();
            fab.animate().translationYBy(150);
        }*/
        if (scrollState == SCROLL_STATE_FLING) {
            if(goingUp) {
                fab.animate().cancel();
                fab.animate().translationY(btn_initPosY);
            } else {
                fab.animate().cancel();
                fab.animate().translationYBy(150);
            }
        }
        /*if() {
            fab.animate().cancel();
            fab.animate().translationY(btn_initPosY);
        }*/
    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        int btn_initPosY=fab.getScrollY();
        if(mLastFirstVisibleItem<firstVisibleItem)
        {
            goingUp = false;
        }
        if(mLastFirstVisibleItem>firstVisibleItem)
        {
            goingUp = true;
        }
        mLastFirstVisibleItem=firstVisibleItem;
    }
}
