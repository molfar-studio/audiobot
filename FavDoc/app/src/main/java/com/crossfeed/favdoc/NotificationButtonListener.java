package com.crossfeed.favdoc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by Roman on 07.08.2017.
 */

public class NotificationButtonListener extends BroadcastReceiver {

    AudioHelper audioHelper = MainActivity.getClickListener().getAudioHelper();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getStringExtra("action");
        switch (action) {
            case "close":
                if (audioHelper.getIsSongConnected()) {
                    //audioHelper.mediaPlayerService.cancelNotification();
                    audioHelper.releaseMp3();
                    audioHelper.stopService();

                    Intent intentRelease = new Intent(AudioHelper.BROADCAST_ACTION);
                    intentRelease.putExtra(AudioHelper.PARAM_TASK, AudioHelper.ON_RELEASE);
                    context.sendBroadcast(intentRelease);
                }
                break;
            case "pause":
                audioHelper.stopMp3();
                audioHelper.mediaPlayerService.initNotification();
                break;
            case "play":
                audioHelper.playMp3();
                audioHelper.mediaPlayerService.initNotification();
                break;
            case "next":
                audioHelper.playNext();
                audioHelper.mediaPlayerService.initNotification();
                break;
            case "previous":
                audioHelper.playPrevious();
                audioHelper.mediaPlayerService.initNotification();
                break;
            default:
                Toast.makeText(context, "FATAL_ERROR", Toast.LENGTH_SHORT).show();
        }


    }
}
