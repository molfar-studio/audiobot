package com.crossfeed.favdoc;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by ivan on 18.08.17.
 */

 public class ActionModeCallback
        implements ListView.MultiChoiceModeListener {
    String selected = " selected";
    ActionMode actionMode;
    ListView playlistFavs;
    PlayListAdapter adapter;
    AudioHelper audioHelper;
    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    SharedPreferences.Editor playlistEdit;
    SharedPreferences playlistPrefs;
    SharedPreferences favsPrefs;
    SharedPreferences.Editor favsEdit;
    SharedPreferences.Editor listEdit;
    Context context;
    int flag;
    FloatingActionButton fab;
    EditText search;
    Activity activity;
    String playlistName;

    public ActionModeCallback(ListView list, Activity act, SharedPreferences prefs, FloatingActionButton fab, EditText searchSelect, String playlistName,  int flag) {
        playlistFavs = list;
        adapter = (PlayListAdapter) list.getAdapter();
        audioHelper =  MainActivity.getClickListener().getAudioHelper();
        this.prefs = prefs;
        activity = act;
        this.context = act.getApplicationContext();
        playlistPrefs = context.getSharedPreferences("playListsSize",0);
        favsPrefs = context.getSharedPreferences("favs", 0);
        this.flag = flag;
        this.fab = fab;
        this.playlistName = playlistName;
        search = searchSelect;
    }


    @Override
    public void onItemCheckedStateChanged(android.view.ActionMode mode, int i, long l, boolean b) {
        final int checked = adapter.getSelectedCount();
        mode.setTitle(checked+selected);
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();

        if(flag == 0)
            inflater.inflate(R.menu.select_menu, menu);
        else if(flag==1)
            inflater.inflate(R.menu.delete_playlist_menu, menu);
        else
            inflater.inflate(R.menu.fill_playlist_menu, menu);

        adapter.actionMode = mode;
        fab.setVisibility(View.GONE);

        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        final int checked = playlistFavs.getCheckedItemCount();
        mode.setTitle(checked+selected);

        if(search!=null) {
            search.setVisibility(View.VISIBLE);

            search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(!s.equals("")) {
                        int pos = adapter.findMatchPos(search.getText().toString());
                        playlistFavs.setSelection(pos);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
        }

        return true;
    }

    @Override
    public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_select:

                if(prefs!=null) {
                    int changed = putIntoPlaylist(prefs);
                    if(playlistName!=null)
                        changePlaylistSize(adapter.playlistName, changed, true);
                }
                mode.finish();
                break;
            case R.id.action_delete:
                SparseBooleanArray selected;
                selected = adapter.getSelectedIds();
                int size  = adapter.getSelectedCount();
                edit = prefs.edit();

                if(flag==1) {
                    for (int i = 0; i < size; i++) {
                        if (selected.valueAt(i)) {
                            edit.putInt(((AudioItem) adapter.getItem(selected.keyAt(i))).link, -1);
                        }
                    }
                    if(playlistName!=null)
                        changePlaylistSize(playlistName, size, false);
                    edit.apply();
                }
                else if(flag == 0) {
                    playlistEdit = prefs.edit();
                    favsEdit = favsPrefs.edit();
                    ArrayList<AudioItem> items2remove = new ArrayList<>();

                    for (int i = 0; i < size; i++) {
                        if (selected.valueAt(i)) {
                            AudioItem song = (AudioItem) adapter.getItem(selected.keyAt(i));
                            if(song.isPlaylist) {
                                playlistEdit.putInt(song.name, -1);
                               // playlistEdit.clear();
                                items2remove.add(song);
                            }
                            else {
                                File file = new File(song.link);
                                if (file.delete()) {
                                    favsEdit.putString(song.ownerId+"_"+song.id, "");
                                    items2remove.add(song);
                                }
                            }
                        }
                    }
                    playlistEdit.apply();
                    favsEdit.apply();
                    adapter.remove(items2remove);
                    Toast.makeText(context, size+" item(-s) deleted", Toast.LENGTH_LONG).show();
                }
                adapter.notifyDataSetChanged();

               /* if(flag ==1) {
                    playlistEdit = playlistPrefs.edit();
                    playlistEdit.putInt(adapter.playlistName, prefs.getInt(adapter.playlistName, 0) - size);
                    playlistEdit.apply();
                }*/

                mode.finish();
                break;
            case R.id.action_add:
                final Dialog dialog = new Dialog(activity);
                dialog.setContentView(R.layout.playlist_choice_dialog);
                dialog.setTitle(context.getResources().getString(R.string.popup_choose));

                ListView playlistChoice = (ListView) dialog.findViewById(R.id.playlist_choice);
                Button createPlay = (Button) dialog.findViewById(R.id.add_playlist_choice);
                PlayListAdapter adapterPlay = new PlayListAdapter(audioHelper.getPlaylists(), context);
                playlistChoice.setAdapter(adapterPlay);

                createPlay.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                            AlertDialog.Builder nameDialog = new AlertDialog.Builder(activity);
                            nameDialog.setTitle(context.getResources().getString(R.string.popup_new));
                            nameDialog.setIcon(R.drawable.ic_playlist_add_check_black_24dp);

                            final EditText input = new EditText(activity);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.MATCH_PARENT);
                            input.setLayoutParams(lp);
                            input.requestFocus();
                            input.setHint(context.getResources().getString(R.string.popup_name));
                            nameDialog.setView(input);

                            nameDialog.setPositiveButton(context.getResources().getString(R.string.popup_btn_create), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface nameDialog, int which) {
                                    String playTitle = input.getText().toString();
                                    if(playTitle.equals(""))
                                        nameDialog.cancel();
                                    context.getSharedPreferences("playLists2",0).edit()
                                            .putLong(playTitle,System.currentTimeMillis()).apply();
                                    edit = playlistPrefs.edit();
                                    edit.putInt(playTitle, 0);
                                    edit.apply();
                                    putIntoPlaylist(context.getSharedPreferences(playTitle, 0));
                                    changePlaylistSize(playTitle, adapter.getSelectedCount(), true);
                                    dialog.cancel();
                                    nameDialog.cancel();
                                    Toast.makeText(context, context.getResources().getString(R.string.popup_added) + playTitle, Toast.LENGTH_LONG).show();
                                    mode.finish();
                                }
                            });
                            nameDialog.show();
                        }
                        return true;
                    }
                });

                playlistChoice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int pos, long l) {
                        AudioItem item = (AudioItem) parent.getItemAtPosition(pos);
                        SharedPreferences playPrefs = context.getSharedPreferences(item.name, 0);
                        int changed = putIntoPlaylist(playPrefs);
                        changePlaylistSize(item.name, changed, true);
                        Toast.makeText(context, " item(-s) added to " + item.name, Toast.LENGTH_LONG).show();
                        dialog.cancel();
                        mode.finish();
                    }
                });
                dialog.show();
                //mode.finish();
                break;
        }

        return true;
    }

    private void changePlaylistSize(String playlistName, int size, boolean add) {
        listEdit = playlistPrefs.edit();

        if(add)
            listEdit.putInt(playlistName, playlistPrefs.getInt(playlistName,0)+size);
        else
            listEdit.putInt(playlistName, playlistPrefs.getInt(playlistName,0)-size);

        listEdit.apply();
    }

    private int putIntoPlaylist(SharedPreferences playlistPref) {
        int changed = 0;
        SparseBooleanArray selected;
        edit = playlistPref.edit();
        selected = adapter.getSelectedIds();
        int size  = adapter.getSelectedCount();
        for(int i=0;i<size;i++) {
            if(selected.valueAt(i)) {
                String link = ((AudioItem)adapter.getItem(selected.keyAt(i))).link;
                if(playlistPref.getInt(link, -1)==-1)
                    changed++;
                edit.putInt(link, i);
            }
        }
        edit.apply();
        return  changed;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        playlistFavs.clearChoices();
         playlistFavs.setChoiceMode(ListView.CHOICE_MODE_NONE);
         adapter.actionMode=null;
         adapter.removeSelection();
        if(search !=null)
        search.setVisibility(View.GONE);

         if(flag!=0)
            adapter.setPlaylistDataSet(prefs);
         fab.setVisibility(View.VISIBLE);
    }
}
