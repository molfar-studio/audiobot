package com.crossfeed.favdoc;

import android.content.Context;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ivan on 03.08.17.
 */

public class VkApi extends Api {
    private String apiDomain = "http://api.xn--41a.ws/api.php?method=";
    private String apiKey = "52d92501db1cc3db6a619ec53619c3dd";
    static final int SEARCH_LIST_MARK = 0;
    Context context;

    public VkApi()  {
        //this.context = context;
    }

    public String getStringSearchRequest(String q, int offset) {
        q = q.replaceAll(" ", "_");
        StringBuilder stringBuilder = new StringBuilder();
        return stringBuilder.append(apiDomain).append("search").append("&q=")
                .append(q).append("&offset=").append(offset).append("&key=").append(apiKey).toString();
    }

    public String getAudioByIdsRequest(ArrayList<String> ids) {
        StringBuilder id = new StringBuilder();
        StringBuilder stringBuilder = new StringBuilder();

        if(ids.size()<1)
            return null;

        int limit = ids.size()>10? 10: ids.size();

        id.append(ids.get(0));
        for(int i=1;i<limit;i++) {
            id.append(",");
            id.append(ids.get(i));
        }
        String idsString = id.toString();

        return stringBuilder.append(apiDomain).append("get.audio").append("&ids=")
                .append(idsString).append("&key=").append(apiKey).toString();
    }

    public String getAudioByIdsRequest(String id) {
        StringBuilder stringBuilder = new StringBuilder();
        return stringBuilder.append(apiDomain).append("get.audio").append("&ids=")
                .append(id).append("&key=").append(apiKey).toString();
    }


    public ArrayList<AudioItem> getAudioBySearch(String searchUrl) {

        String content = getResponseText(searchUrl);
       // Toast.makeText(context,content, Toast.LENGTH_SHORT).show();

        ArrayList<AudioItem> audios = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(content);
            JSONArray jsonArray = jsonObject.getJSONArray("list");

            for (int i=0; i<jsonArray.length(); i++) {
             AudioItem item = new AudioItem();
             JSONArray jsonItem = jsonArray.getJSONArray(i);

             item.list_mark = SEARCH_LIST_MARK;
             item.id = jsonItem.getLong(0);
             item.ownerId = jsonItem.getLong(1);
             item.author = jsonItem.getString(4);
             item.name = jsonItem.getString(3);
             //item.setLink(jsonItem.getString(2));

             audios.add(item);
            }
        }
        catch (JSONException e) {
            //Toast.makeText(context,"json api getIdsBySearch", Toast.LENGTH_SHORT).show();
        }
        return audios;
    }

    public ArrayList<String> getIdsBySearch(String searchRequest) {
        String content = getResponseText(searchRequest);
        // Toast.makeText(context,content, Toast.LENGTH_SHORT).show();

        ArrayList<String> ids = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(content);
            JSONArray jsonArray = jsonObject.getJSONArray("list");

            for (int i=0; i<jsonArray.length(); i++) {
                JSONArray jsonItem = jsonArray.getJSONArray(i);
                String id = jsonItem.getLong(1)+"_"+jsonItem.getLong(0);

                ids.add(id);
            }
        }
        catch (JSONException e) {
            //Toast.makeText(context,"json api getIdsBySearch", Toast.LENGTH_SHORT).show();
        }
        return ids;
    }

    public ArrayList<AudioItem> getAudioByIds(String request) {
        String content = getResponseText(request);
        ArrayList<AudioItem> audios = new ArrayList<>();

        try {
            JSONArray jsonArray = new JSONArray(content);

            for (int i=0; i<jsonArray.length(); i++) {
                AudioItem item = new AudioItem();
                JSONArray jsonItem = jsonArray.getJSONArray(i);

                item.list_mark = SEARCH_LIST_MARK;
                item.id = jsonItem.getLong(0);
                item.ownerId = jsonItem.getLong(1);
                item.author = jsonItem.getString(4);
                item.name = jsonItem.getString(3);
                item.link = jsonItem.getString(2);
                item.duration = jsonItem.getInt(5)*1000;
                item.picUrl = jsonItem.getString(14);
                item.picUrl = item.picUrl.contains(",")? item.picUrl.split(",")[0]: item.picUrl;

                audios.add(item);
            }
        }
        catch (JSONException e) {
            //Toast.makeText(context,"json api getIdsBySearch", Toast.LENGTH_SHORT).show();
        }
        return audios;
    }

    public String getAudiosLink(String request) {
        String content = getResponseText(request);

        ArrayList<AudioItem> audios = new ArrayList<>();
        String link = "";
        try {
            JSONArray jsonArray = new JSONArray(content);

            JSONArray jsonItem = jsonArray.getJSONArray(0);
            link = jsonItem.getString(2);


        }
        catch (JSONException e) {
            //Toast.makeText(context,"json api getAudiosBySearch", Toast.LENGTH_SHORT).show();
        }
        return link;
    }

}
