package com.crossfeed.favdoc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by ivan on 26.08.17.
 */

public class Api {

    public String getResponseText(String urlText) {
        StringBuilder response = new StringBuilder();
        try {
            URL url = new URL(urlText);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();

            if(conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String current_line ;
                while((current_line = reader.readLine())!= null) {
                    response.append(current_line);
                }
                reader.close();
            }
            else
                response.append("| response code is invalid");
        }
        catch(MalformedURLException e) {
            response.append("| invalid url exception was thrown");
        }
        catch(IOException e) {
            response.append("| error while getting the response");
        }
        return  response.toString();
    }
}
