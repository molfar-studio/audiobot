package com.crossfeed.favdoc;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;

/**
 * Created by ivan on 26.07.17.
 */

public class CurrentPlayInfo {
    int ACTIVE_LIST_MARK;
    String activeLink;
    String activeName;
    SharedPreferences prefs;
    int duration;
    long id, ownerId;
    String activeAuthorName;
    AudioItem currentSong;

    public CurrentPlayInfo(Context context) {
        prefs = context.getSharedPreferences("favs", 0);
        activeLink = "";
        activeName = "";
        ACTIVE_LIST_MARK = -1;
        duration=0;
        currentSong = null;
    }

    public boolean isFav() {
        String link = prefs.getString(ownerId+"_"+id, "");
        return !"".equals(link);
    }

  /*  public int getActiveListMark() {
        return ACTIVE_LIST_MARK;
    }

    public void setActiveListMark(int activeListMark) {
        ACTIVE_LIST_MARK = activeListMark;
    }

    public String getActiveLink() {
        return activeLink;
    }

    public void setActiveLink(String activeLink) {
        this.activeLink = activeLink;
    }

    public String getActiveName() {
        return activeName;
    }

    public void setActiveName(String activeName) {
        this.activeName = activeName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public String getActiveAuthorName() {
        return activeAuthorName;
    }

    public void setActiveAuthorName(String activeAuthorName) {
        this.activeAuthorName = activeAuthorName;
    }*/

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo!=null && networkInfo.isConnectedOrConnecting();
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

}
