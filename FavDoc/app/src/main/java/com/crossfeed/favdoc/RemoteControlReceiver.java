package com.crossfeed.favdoc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;

/**
 * Created by ivan on 07.09.17.
 */

public class RemoteControlReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_MEDIA_BUTTON.equals(intent.getAction())) {
            KeyEvent event = (KeyEvent) intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);

            //Toast.makeText(context, "mediaButton", Toast.LENGTH_SHORT).show();

            if (event == null) {
                return;
            }

            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                Intent pauseIntent = new Intent(AudioHelper.BROADCAST_ACTION);
                pauseIntent.putExtra(AudioHelper.PARAM_TASK, AudioHelper.ON_MEDIA_BUTTON_PRESSED);
                context.sendBroadcast(pauseIntent);
            }
        }
    }

}