package com.crossfeed.favdoc;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by hawk on 04.01.2017.
 */
public class BrowserInfo {
    ArrayList<String> hosts;
    ArrayList<String> links;
    String user_agent;
    final String DOMAIN_NAME_PATTERN = "https?://([a-zA-Z0-9]([a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])?\\.)+[a-zA-Z]{2,6}";
    final String GOOGLE_LINK_MARKER = "/url?q=";
    Pattern domain_pattern_name;
    final String cache_marker = "webcache";
    final String[] useless_urls = {"youtube", "https://www.google",
            "https://vk",};

    public BrowserInfo() {
        this.user_agent = "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)";
        domain_pattern_name = Pattern.compile(DOMAIN_NAME_PATTERN);
    }

   public String getCacheMarker() {
       return cache_marker;
   }
    public Pattern getDomainPatternName() {
        return domain_pattern_name;
    }
    public String getUserAgent() {
        return user_agent;
    }
    public String getGoogleLinkMarker() {
        return GOOGLE_LINK_MARKER;
    }

    public void setLinks(ArrayList<String> links) {
        this.links = links;
    }
    public ArrayList<String> getLinks() {
        return links;
    }

    public void setHosts(ArrayList<String> hosts) {
        this.hosts = hosts;
    }
    public ArrayList<String> getHosts() {
        return hosts;
    }

}
