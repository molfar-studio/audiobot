package com.crossfeed.favdoc;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import net.rdrei.android.dirchooser.DirectoryChooserActivity;
import net.rdrei.android.dirchooser.DirectoryChooserConfig;

import static android.os.Environment.DIRECTORY_MUSIC;

/**
 * Created by ivan on 29.08.17.
 */

public class SettingsActivity extends AppCompatPreferenceActivity {
    private static final String TAG = SettingsActivity.class.getSimpleName();
    private static final int REQUEST_DIRECTORY = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // load settings fragment
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MainPreferenceFragment()).commit();
    }

    public static class MainPreferenceFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_main);

            PreferenceManager.getDefaultSharedPreferences(getActivity()).registerOnSharedPreferenceChangeListener(this);

            Preference pref = findPreference("storage_path");
            pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!Settings.System.canWrite(getActivity())) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE}, 2909);
                        }
                    }

                    Intent chooserIntent = new Intent(getActivity(), DirectoryChooserActivity.class);
                    DirectoryChooserConfig config = DirectoryChooserConfig.builder()
                            .newDirectoryName("AudioBot_Music")
                            .allowReadOnlyDirectory(false)
                            .allowNewDirectoryNameModification(true)
                            .initialDirectory(MainActivity.path)
                            .build();
                    chooserIntent.putExtra(DirectoryChooserActivity.EXTRA_CONFIG, config);
                    getActivity().startActivityForResult(chooserIntent, REQUEST_DIRECTORY);
                    return true;
                }
            });
            SharedPreferences sPref = getActivity().getSharedPreferences("path", 0);
            pref.setSummary(sPref.getString("storage_path_string", getActivity().getExternalFilesDir(DIRECTORY_MUSIC).getAbsolutePath()));
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

            if (key.equals("theme_list")) {
                SharedPreferences theme_preference = getActivity().getApplicationContext().getSharedPreferences("theme", 0);
                SharedPreferences.Editor ed = theme_preference.edit();
                switch (Integer.parseInt(sharedPreferences.getString("theme_list", "0"))) {
                    case 0: // Default theme
                        ed.putInt("theme_code", 0);
                        break;

                    case 1: // Dark theme
                        ed.putInt("theme_code", 1);
                        break;
                }
                ed.apply();
                //MainActivity.settings_changed = true;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 2909: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Permission", "Granted");
                } else {
                    Log.e("Permission", "Denied");
                    Toast.makeText(getApplicationContext(), "You need to give access to change folder location", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            String new_path = data.getStringExtra(DirectoryChooserActivity.RESULT_SELECTED_DIR);
            SharedPreferences sPref = getSharedPreferences("path", 0);
            SharedPreferences.Editor ed = sPref.edit();
            ed.putString("storage_path_string", new_path);
            ed.apply();
            MainActivity.path = new_path;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }




}
