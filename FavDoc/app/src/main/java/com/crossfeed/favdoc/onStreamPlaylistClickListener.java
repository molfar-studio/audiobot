package com.crossfeed.favdoc;


import android.content.ServiceConnection;
import android.view.View;
import android.widget.AdapterView;

/**
 * Created by hawk on 09.01.2017.
 */
public class onStreamPlaylistClickListener implements AdapterView.OnItemClickListener {
    private AudioHelper audioHelper;
    CurrentPlayInfo playInfo;

    ServiceConnection sConn;
    MediaPlayerService mediaPlayerService;
    boolean bound = false;

    public onStreamPlaylistClickListener(AudioHelper audioHelper, MediaPlayerService mediaPlayerService) {
        playInfo = MainActivity.getPlayInfo();
        this.audioHelper = audioHelper;
        this.mediaPlayerService = mediaPlayerService;

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        AudioItem song = (AudioItem)parent.getItemAtPosition(position);
        String link = song.link;

        if (link.equals(playInfo.activeLink) &&
                song.list_mark == playInfo.ACTIVE_LIST_MARK) {

            if (audioHelper.getIsPlaying()) {
                audioHelper.stopMp3();
            }
            else if(audioHelper.getIsSongConnected()){
                audioHelper.playMp3();
            }
            else
                audioHelper.releaseMp3();
        }
        else {
            /*if (!CurrentPlayInfo.isNetworkConnected(context)) {
                Toast.makeText(context,
                        R.string.network_access_error_text, Toast.LENGTH_SHORT).show();
                return;
            }*/
            audioHelper.setFocusParams(position, view);
            audioHelper.setParent(parent);
/*
            if(audioHelper!=null) {
                audioHelper.releaseMp3();
                audioHelper.setActivePlaylist(((PlayListAdapter)parent.getAdapter()).getActivePlaylist());
                audioHelper.setActivePlaylistIndex(((PlayListAdapter)parent.getAdapter()).getActivePlaylist().indexOf(parent.getItemAtPosition(position)));
                audioHelper.setConnectionMp3((AudioItem) parent.getItemAtPosition(position),
                        ((PlayListAdapter)parent.getAdapter()).getActivePlaylist(), view);
            }*/
        }

    }

    public AudioHelper getAudioHelper () {
        return audioHelper;
    }
    public MediaPlayerService getMediaPlayerService() {
        return mediaPlayerService;
    }
    public boolean isBound() {
        return bound;
    }

}
