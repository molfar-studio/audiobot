package com.crossfeed.favdoc;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.os.Environment.DIRECTORY_MUSIC;
import static com.crossfeed.favdoc.AudioHelper.BROADCAST_ACTION;
import static com.crossfeed.favdoc.AudioHelper.ON_COMPLETION_TASK;
import static com.crossfeed.favdoc.AudioHelper.PARAM_TASK;

/*import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.media.MediaMetadata;*/
/*=======
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
>>>>>>> 37acff8ec4fb089fab93e3258b793e42d8f8c3e0*/

/**
 * Created by ivan on 21.07.17.
 */

public class PlayerActivity extends AppCompatActivity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    AudioHelper audioHelper;
    //BottomPlayer bottomPlayer;
    CurrentPlayInfo playInfo;
    //CurrentPlayInfo favsPlayInfo;
    Button play,/* addFav,*/
            skipNext, skipPrev, repeatBtn, shuffleBtn;
    Toolbar toolbar;
    SeekBar seekbar;
    TextView play_time, duration_time;
    Handler handler;
    DownloadManager downloadManager;
    AudioItem song;
    BroadcastReceiver br;
    ImageView art;
    Menu menu;
    SharedPreferences playlistPrefs;
    static final int PICTURE_REQUEST_CODE = 1;

    int duration, progress;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {

            if (progress < duration - 200)
                progress = audioHelper.getMp3Progress();
            else
                return;

            if (progress != -1) {
                seekbar.setProgress(progress);
                play_time.setText(progress / 60000 + ((progress / 1000) % 60 < 10 ? ":0" : ":") + (progress / 1000) % 60);
            }
            //if (audioHelper.getIsPlaying())
            //    play.setBackgroundResource(R.drawable.ic_pause);
            // else if (audioHelper.getIsSongConnected())
            //  play.setBackgroundResource(R.drawable.ic_play_arrow);

            handler.postDelayed(this, 100);
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        switch (MainActivity.theme_code) {
            case 0:
                break;
            case 1:
                setTheme(R.style.PlayerTheme_Dark);
                break;
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        play_time = (TextView) findViewById(R.id.song_playing_time);
        duration_time = (TextView) findViewById(R.id.song_duration_time);

        switch (MainActivity.theme_code) {
            case 0:
                play_time.setTextColor(getResources().getColor(R.color.brightBlue));
                duration_time.setTextColor(getResources().getColor(R.color.brightBlue));
                break;
            case 1:
                play_time.setTextColor(getResources().getColor(R.color.orange));
                duration_time.setTextColor(getResources().getColor(R.color.orange));
                break;
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_player);
        RelativeLayout bottom_tools = (RelativeLayout) findViewById(R.id.bottomTools);
        RelativeLayout root_layout = (RelativeLayout) findViewById(R.id.root_layout);
        switch (MainActivity.theme_code) {
            case 0:
                toolbar.setBackgroundColor(getResources().getColor(R.color.white));
                bottom_tools.setBackgroundColor(getResources().getColor(R.color.white));
                root_layout.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                break;
            case 1:
                toolbar.setBackgroundColor(getResources().getColor(R.color.ab_gray));
                bottom_tools.setBackgroundColor(getResources().getColor(R.color.ab_gray));
                break;
        }

        song = (AudioItem) getIntent().getSerializableExtra("song");

        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int task = intent.getIntExtra(PARAM_TASK, 0);
                switch (task) {
                    case ON_COMPLETION_TASK:
                        updatePlayer();
                        break;
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter(BROADCAST_ACTION);
        registerReceiver(br, intentFilter);

        playlistPrefs = getSharedPreferences("playLists2", 0);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/ubuntu.regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );


        onStreamPlaylistClickListener clickListener = MainActivity.getClickListener();
        audioHelper = clickListener.getAudioHelper();

        handler = new Handler();

        seekbar = (SeekBar) findViewById(R.id.seek_bar);
        //seekbar.getProgressDrawable().setColorFilter(
        //        new PorterDuffColorFilter(getResources().getColor(R.color.brightBlue), PorterDuff.Mode.SRC_ATOP));
        //seekbar.getIndeterminateDrawable().setColorFilter(
        //     new PorterDuffColorFilter(getResources().getColor(R.color.brightBlue), PorterDuff.Mode.SRC_ATOP));

        //bottomPlayer = MainActivity.getBottomPlayer();
        playInfo = MainActivity.getPlayInfo();
        /*favsPlayInfo = new CurrentPlayInfo(this);
        favsPlayInfo.id = playInfo.id;
        favsPlayInfo.ownerId = playInfo.ownerId;*/

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_keyboard_backspace_black_24dp));
        setSupportActionBar(toolbar);
        art = (ImageView) findViewById(R.id.art_player);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        play = (Button) findViewById(R.id.playBt);
        // addFav = (Button) findViewById(R.id.add_fav_bt);

        skipNext = (Button) findViewById(R.id.skipNextBt);
        skipPrev = (Button) findViewById(R.id.skipPreviousBt);

        repeatBtn = (Button) findViewById(R.id.repeat_btn);
        shuffleBtn = (Button) findViewById(R.id.shuffle_btn);

        //addFav.setOnClickListener(this);
        play.setOnClickListener(this);

        skipNext.setOnClickListener(this);
        skipPrev.setOnClickListener(this);

        repeatBtn.setOnClickListener(this);
        shuffleBtn.setOnClickListener(this);

        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        updatePlayer();


       /* seekbar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(!audioHelper.getIsConnecting()) {
                    audioHelper.seekMp3(seekbar.getProgress());
                }
                return false;
            }
        });*/

        seekbar.setOnSeekBarChangeListener(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (br != null)
            unregisterReceiver(br);
    }

    private void updatePlayer() {

        invalidateOptionsMenu();

        if (playInfo != null && audioHelper != null) {

            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(playInfo.activeName);
                getSupportActionBar().setSubtitle(playInfo.activeAuthorName);
            }

            if (audioHelper.getIsPlaying())
                play.setBackgroundResource(R.drawable.ic_pause);
            else if (audioHelper.getIsSongConnected())
                play.setBackgroundResource(R.drawable.ic_play_arrow);


            switch (audioHelper.isCycled()) {
                case 0:
                    repeatBtn.setBackgroundResource(R.drawable.ic_repeat_black_24dp);
                    break;
                case 1:
                    repeatBtn.setBackgroundResource(R.drawable.ic_repeat_blue_24dp);
                    break;
                case 2:
                    repeatBtn.setBackgroundResource(R.drawable.ic_repeat_one_blue_24dp);
                    break;
            }

            Picasso.with(this).load(Uri.parse(playInfo.currentSong.picUrl)).into(art);

            if (audioHelper.isShuffled()) {
                shuffleBtn.setBackgroundResource(R.drawable.ic_shuffle_blue_24dp);
            } else {
                shuffleBtn.setBackgroundResource(R.drawable.ic_shuffle_black_24dp);
            }

            progress = 0;
            seekbar.setProgress(progress);
            play_time.setText(progress / 60000 + ((progress / 1000) % 60 < 10 ? ":0" : ":") + (progress / 1000) % 60);

            duration = playInfo.duration;
            seekbar.setMax(duration - 200);
            duration_time.setText(duration / 60000 + ((duration / 1000) % 60 < 10 ? ":0" : ":") + (duration / 1000) % 60);

            handler.postDelayed(runnable, 100);
        }
    }

    private void downloadUrl(String url2download, final String title, final AudioItem song) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url2download));
        request.setDescription(title);
        request.setTitle("favdoc");
        final String artUrl = song.picUrl;

        request.allowScanningByMediaScanner();
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        if (playInfo.isExternalStorageWritable()) {
            //request.setDestinationInExternalPublicDir("/favdoc", title);
            request.setDestinationInExternalFilesDir(this, DIRECTORY_MUSIC, title);
        } else {
            String directoryPath = Environment.getDataDirectory().getPath() + "/favdoc";
            request.setDestinationUri(Uri.fromFile(new File(directoryPath + title)));
        }

        BroadcastReceiver onComplete = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                File src = new File(MainActivity.path + File.separator + title);
                audioHelper.putImagePref(src.getAbsolutePath(), artUrl);
                //unregisterReceiver(this);
            }
        };

        //registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        audioHelper.putImagePref(getExternalFilesDir(DIRECTORY_MUSIC).getAbsolutePath() + File.separator + title, artUrl);
        downloadManager.enqueue(request);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.playBt:
                if (!audioHelper.getIsConnecting()) {
                    if (audioHelper.getIsPlaying()) {
                        audioHelper.stopMp3();
                        view.setBackground(getResources().getDrawable(R.drawable.ic_play_arrow));
                    } else {
                        audioHelper.playMp3();
                        view.setBackground(getResources().getDrawable(R.drawable.ic_pause));
                    }
                }
                break;

            case R.id.skipNextBt:
                audioHelper.playNext();
                updatePlayer();
                break;

            case R.id.skipPreviousBt:
                audioHelper.playPrevious();
                updatePlayer();
                break;

            case R.id.repeat_btn:
                switch (audioHelper.isCycled()) {
                    case 0:
                        audioHelper.setIsCycled(1);
                        repeatBtn.setBackgroundResource(R.drawable.ic_repeat_blue_24dp);
                        break;
                    case 1:
                        audioHelper.setIsCycled(2);
                        repeatBtn.setBackgroundResource(R.drawable.ic_repeat_one_blue_24dp);
                        break;
                    case 2:
                        audioHelper.setIsCycled(0);
                        repeatBtn.setBackgroundResource(R.drawable.ic_repeat_black_24dp);
                        break;
                }
                break;

            case R.id.shuffle_btn:
                if (audioHelper.isShuffled()) {
                    shuffleBtn.setBackgroundResource(R.drawable.ic_shuffle_black_24dp);
                    audioHelper.setIsShuffled(false);
                } else {
                    shuffleBtn.setBackgroundResource(R.drawable.ic_shuffle_blue_24dp);
                    audioHelper.setIsShuffled(true);
                }
                break;

            case R.id.add_fav_bt:
                if (!audioHelper.getIsConnecting()) {

                    if (/*playInfo.isFav() ||*/ song.list_mark == MainActivity.FAVS_LIST_MARK) {

                        File file = new File(playInfo.activeLink);
                        if (file.delete()) {
                            audioHelper.putFavPrefs(playInfo.ownerId + "_" + playInfo.id, "");
                            view.setBackground(getResources().getDrawable(R.drawable.ic_file_download_black));

                            // if(playInfo.getActiveListMark() == MainActivity.FAVS_LIST_MARK) {
                            //}

                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_remove_favs), Toast.LENGTH_SHORT).show();
                            finish();
                        } // else
                        //Toast.makeText(getApplicationContext(), "cannot remove from favs", Toast.LENGTH_SHORT).show();
                    } else {
                        audioHelper.putFavPrefs(playInfo.ownerId + "_" + playInfo.id, playInfo.ownerId + "_" + playInfo.id);
                        view.setBackground(getResources().getDrawable(R.drawable.ic_delete_blue_24dp));

                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_add_favs), Toast.LENGTH_SHORT).show();

                        downloadUrl(playInfo.activeLink, song.author + "-" + song.name + ".mp3", song);
                    }
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        if (playInfo.ACTIVE_LIST_MARK == MainActivity.FAVS_LIST_MARK)
            getMenuInflater().inflate(R.menu.activity_player_menu, menu);
        else
            getMenuInflater().inflate(R.menu.menu_player_search, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.download:
                item.setIcon(R.drawable.ic_file_download_blue);
                audioHelper.putFavPrefs(playInfo.ownerId + "_" + playInfo.id, playInfo.ownerId + "_" + playInfo.id);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_add_favs), Toast.LENGTH_SHORT).show();

                downloadUrl(playInfo.activeLink, song.author + "-" + song.name, song);
                break;
            case R.id.delete_song:

                final AlertDialog.Builder deleteDialog = new AlertDialog.Builder(this);
                deleteDialog.setTitle(getResources().getString(R.string.popup_delete) + playInfo.activeName + "?");
                deleteDialog.setPositiveButton(getResources().getString(R.string.popup_btn_delete), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface nameDialog, int which) {
                        File file = new File(playInfo.activeLink);
                        if (file.delete()) {
                            audioHelper.putFavPrefs(playInfo.ownerId + "_" + playInfo.id, "");

                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_remove_favs), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        nameDialog.cancel();
                        finish();
                    }
                });
                deleteDialog.setNegativeButton(getResources().getString(R.string.popup_btn_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface nameDialog, int which) {
                        nameDialog.cancel();
                    }
                });
                deleteDialog.show();
                break;
            case R.id.addThisToPlaylist:
                final Dialog addDialog = new Dialog(this);
                addDialog.setContentView(R.layout.playlist_choice_dialog);
                addDialog.setTitle(getResources().getString(R.string.popup_choose));

                ListView playlistChoice = (ListView) addDialog.findViewById(R.id.playlist_choice);
                Button createPlay = (Button) addDialog.findViewById(R.id.add_playlist_choice);
                PlayListAdapter adapter = new PlayListAdapter(audioHelper.getPlaylists(), PlayerActivity.this);
                playlistChoice.setAdapter(adapter);

                createPlay.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                            AlertDialog.Builder nameDialog = new AlertDialog.Builder(PlayerActivity.this);
                            nameDialog.setTitle(getResources().getString(R.string.popup_new));
                            nameDialog.setIcon(R.drawable.ic_playlist_add_check_black_24dp);

                            final EditText input = new EditText(PlayerActivity.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.MATCH_PARENT);
                            input.setLayoutParams(lp);
                            input.requestFocus();
                            input.setHint(getResources().getString(R.string.popup_name));
                            nameDialog.setView(input);

                            nameDialog.setPositiveButton(getResources().getString(R.string.popup_btn_create), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface nameDialog, int which) {
                                    String playTitle = input.getText().toString();
                                    if (playTitle.equals(""))
                                        nameDialog.cancel();
                                    SharedPreferences.Editor edit = playlistPrefs.edit();
                                    edit.putInt(playTitle, 0);
                                    edit.apply();
                                    putIntoPlaylist(getSharedPreferences(playTitle, 0));
                                    Toast.makeText(PlayerActivity.this, getResources().getString(R.string.popup_added), Toast.LENGTH_LONG).show();
                                    addDialog.cancel();
                                    nameDialog.cancel();
                                }
                            });
                            nameDialog.show();
                        }
                        return true;
                    }
                });

                playlistChoice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int pos, long l) {
                        AudioItem item = (AudioItem) parent.getItemAtPosition(pos);
                        SharedPreferences playPrefs = getApplicationContext().getSharedPreferences(item.name, 0);
                        putIntoPlaylist(playPrefs);
                        Toast.makeText(PlayerActivity.this, getResources().getString(R.string.popup_added) + item.name, Toast.LENGTH_LONG).show();
                        addDialog.cancel();
                    }
                });
                addDialog.show();
                break;
          /*  case R.id.to_top:
                if (playInfo.ACTIVE_LIST_MARK == MainActivity.FAVS_LIST_MARK) {
                    File file = new File(playInfo.activeLink);

                    file.setLastModified(SystemClock.currentThreadTimeMillis());

                    try {
                        FileUtils.touch(file);
                    }
                    catch (IOException e) {

                    }
                }
                break;*/
            case R.id.set_pic:
                Intent i = new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(i, getResources().getString(R.string.text_file_choose)), PICTURE_REQUEST_CODE);
                break;
            case R.id.delete_pic:
                audioHelper.putImagePref(playInfo.activeLink, "");
                playInfo.currentSong.picUrl = "";
                Picasso.with(this).load(R.drawable.ic_library_music).transform(new CircleTransform()).into(art);
                break;

            case R.id.info:

                Dialog infoDialog = new Dialog(PlayerActivity.this);
                infoDialog.setTitle("Info");
                //infoDialog.setIcon(R.drawable.ic_playlist_add_check_black_24dp);
                infoDialog.setContentView(R.layout.info_audio_popup);

                File thisFile = new File(playInfo.activeLink);

                if (!thisFile.exists())
                    break;

                TextView full_name = (TextView) infoDialog.findViewById(R.id.full_name_value);
                TextView size = (TextView) infoDialog.findViewById(R.id.size_value);
                TextView path = (TextView) infoDialog.findViewById(R.id.path_value);
                TextView lastModified = (TextView) infoDialog.findViewById(R.id.last_modified_value);
                TextView title = (TextView) infoDialog.findViewById(R.id.title_tv);

                title.setText(playInfo.activeName);
                full_name.setText(playInfo.activeName);
                size.setText(thisFile.length() / 1000000 + " mb");
                path.setText(playInfo.activeLink);
                lastModified.setText(getDate(thisFile.lastModified(), "dd/MM/yyyy"));


                infoDialog.show();
                break;

            case R.id.share:
                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.share_app_title));
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, getResources().getString(R.string.share_track_part1) +
                        playInfo.activeName + getResources().getString(R.string.share_track_part2));
                startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.share_via)));
                break;


        }
        return super.onOptionsItemSelected(item);
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    void putIntoPlaylist(SharedPreferences prefs) {
        SharedPreferences.Editor edit = prefs.edit();
        edit.putInt(playInfo.activeLink, 1);
        edit.apply();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

      /*  if (requestCode == PICTURE_REQUEST_CODE && resultCode == RESULT_OK && null != data) {
            Cursor cursor = getContentResolver().query(data.getData(), new String[] {MediaStore.Images.Media.DATA}, null,null,null);
            if(cursor != null && cursor.moveToFirst()) {
                String picturePath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA));
                audioHelper.putImagePref(getExternalFilesDir(
                        DIRECTORY_MUSIC).getAbsolutePath() + File.separator + playInfo.activeName, picturePath);
                cursor.close();
            }
*/
        if (requestCode == PICTURE_REQUEST_CODE && resultCode == RESULT_OK && null != data) {
            Uri selectedImageUri = data.getData();
            String picturePath = getPath(this, selectedImageUri);
            Toast.makeText(this,
                    picturePath, Toast.LENGTH_SHORT).show();
            audioHelper.putImagePref(playInfo.activeLink, "file://" + picturePath);
            playInfo.currentSong.picUrl = "file://" + picturePath;
            Picasso.with(this).load(Uri.parse("file://" + picturePath)).transform(new CircleTransform()).into(art);
        }
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            if (!audioHelper.getIsConnecting()) {
                audioHelper.seekMp3(seekbar.getProgress());
            }
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}





























