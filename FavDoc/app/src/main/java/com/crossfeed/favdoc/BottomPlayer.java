package com.crossfeed.favdoc;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by hawk on 10.01.2017.
 */
public class BottomPlayer implements View.OnClickListener, View.OnTouchListener {
    private AudioItem song;
    private RelativeLayout bottom_player;
    ProgressBar progressBar;
    Button close_bottom, play_bottom;
    AppCompatActivity activity;
    AudioHelper audioHelper;
    Context context;
    HorizontalScrollView scrollView;
    TextView song_title;

    private static int ACTIVE_LIST_MARK;
    private String active_link;
    private int focused_position;
    int search_list_length, favs_list_length;
    boolean isConnected, isPlaying;

    public BottomPlayer(Activity activity) {
        this.activity = (AppCompatActivity) activity;
        context = activity.getApplicationContext();
        //prefs = MainActivity.getContext().getSharedPreferences("favs", 0);
        ACTIVE_LIST_MARK = -1;
        focused_position = -1;
        active_link = "";
        isConnected = false;
        isPlaying = false;

        bottom_player = (RelativeLayout) activity.findViewById(R.id.bottomPlayer);
        progressBar = (ProgressBar) bottom_player.findViewById(R.id.bottom_progressbar);
        close_bottom = (Button) bottom_player.findViewById(R.id.close_bottom);
        play_bottom = (Button) bottom_player.findViewById(R.id.play_bottom);
        song_title = (TextView) bottom_player.findViewById(R.id.songTitle);
        //scrollView = (HorizontalScrollView) bottom_player.findViewById(R.id.song_name_scroll) ;
        song_title.setOnClickListener(this);
        bottom_player.setOnClickListener(this);
        switch (MainActivity.theme_code) {
            case 0:
                break;
            case 1:
                bottom_player.setBackgroundResource(R.color.ac_gray);
                song_title.setTextColor(context.getResources().getColor(R.color.white));
                break;
        }
    }

    public void initialize(AudioItem item, AudioHelper audioHelper) {
        this.song = item;
        this.audioHelper = audioHelper;

        close_bottom.setOnClickListener(this);
        play_bottom.setOnClickListener(this);

       // play_bottom.setBackground(context.getResources().getDrawable(R.drawable.ic_pause_blue));

    }

    public void setBottomPlayer() {
        progressBar = (ProgressBar) bottom_player.findViewById(R.id.bottom_progressbar);
        progressBar.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.blue), PorterDuff.Mode.MULTIPLY);
        String title = song.author+" - "+song.name;
        song_title.setText(title.replace(".mp3", ""));

        bottom_player.setVisibility(View.VISIBLE);

    }


    public void setProgress() {
        if (progressBar != null && play_bottom != null) {
            progressBar.setVisibility(View.VISIBLE);
            play_bottom.setVisibility(View.GONE);
        }
    }

    public void unsetProgress() {
        if (progressBar != null && play_bottom != null) {
            progressBar.setVisibility(View.GONE);
            play_bottom.setVisibility(View.VISIBLE);
        }
    }

    public void setPlaybottom() {
        if(audioHelper.getIsPlaying())
            play_bottom.setBackgroundResource(R.drawable.ic_pause_blue);
        else if(audioHelper.getIsSongConnected())
            play_bottom.setBackgroundResource(R.drawable.ic_play_blue);
        else if(audioHelper.getIsConnecting())
            setProgress();
    }

    public void setPlaylistPlaybottom() {
            play_bottom.setBackgroundResource(R.drawable.ic_pause_blue);
    }

    public void setLayoutVisibility(boolean flag) {
        if (flag)
            bottom_player.setVisibility(View.VISIBLE);
        else
            bottom_player.setVisibility(View.GONE);
    }

    public void setSearchListLength(int length) {
        search_list_length = length;
    }

    public void setFavsListLength(int length) {
        favs_list_length = length;
    }

    public int getListLength(int mark) {
        if (mark == 0)
            return search_list_length;
        else if (mark == 1)
            return favs_list_length;
        else
            return 0;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.close_bottom:
                if (audioHelper.getIsSongConnected()) {
                    audioHelper.releaseMp3();
                    audioHelper.stopService();
                    setLayoutVisibility(false);
                }
                break;

            case R.id.play_bottom:
                if (audioHelper.getIsPlaying()) {
                    audioHelper.stopMp3();
                    view.setBackground(context.getResources().getDrawable(R.drawable.ic_play_blue));
                } else {
                    audioHelper.playMp3();
                    view.setBackground(context.getResources().getDrawable(R.drawable.ic_pause_blue));
                }
                break;
            case R.id.bottomPlayer:
            case R.id.songTitle:
                if (!audioHelper.getIsConnecting()) {
                    Intent intent = new Intent(context, PlayerActivity.class);
                    intent.putExtra("song", song);
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);
                }
                break;

        }
    }


    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_UP){
            switch (view.getId()) {

                case R.id.bottomPlayer:
                case R.id.songTitle:
                    if (!audioHelper.getIsConnecting()) {
                        Intent intent = new Intent(context, PlayerActivity.class);
                        intent.putExtra("song", song);
                        activity.startActivity(intent);
                        activity.overridePendingTransition(R.anim.enter, R.anim.exit);
                    }
                    break;
            }
            return true;
        }
        return false;
    }


}
