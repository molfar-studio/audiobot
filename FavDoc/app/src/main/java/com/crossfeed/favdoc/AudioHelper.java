package com.crossfeed.favdoc;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.Random;

import io.gresse.hugo.vumeterlibrary.VuMeterView;

import static com.crossfeed.favdoc.MainActivity.FAVS_LIST_MARK;

/**
 * Created by hawk on 04.01.2017.
 */
public class AudioHelper implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener {

    private MediaPlayer mediaPlayer;
    private Context context;
    private boolean isSongConnected, isPlaying, isConnecting;
    //private Intent playerService;
    CurrentPlayInfo playInfo;
    Activity activity;

    public final static int ON_PREPARED_TASK = 1;
    public final static int ON_COMPLETION_TASK = 2;
    public final static int ON_PAUSE = 3;
    public final static int ON_PLAY = 4;
    public final static int ON_BUFFER = 5;
    public final static int ON_UNBUFFER = 6;
    public final static int ON_MEDIA_BUTTON_PRESSED = 7;
    public final static int ON_RELEASE = 8;
    public final static String BROADCAST_ACTION = "com.crossfeed.favdoc";
    public final static String PARAM_TASK = "task";
    public boolean isHidden = false;

    private View focused_view;
    BottomPlayer bottomPlayer;
    RelativeLayout player;
    FrameLayout button_holder;
    Button play, bottomPlay;
    ImageView itemPlay;
    Button stop;
    boolean canceled;
    BroadcastReceiver br;
    ArrayList<AudioItem> playList;
    int activePlaylistIndex = -1;
    VkApi vkApi;
    AudioItem song_item;
    VuMeterView vumeter;
    int focused_position;
    AdapterView<?> parent;

    ServiceConnection sConn;
    MediaPlayerService mediaPlayerService;
    boolean bound = false;

    SharedPreferences artPrefs;
    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    public static int [] iconBacks = {Constants.ICON_COLOR.PINK, Constants.ICON_COLOR.PURPLE,
            Constants.ICON_COLOR.DEEP_PURPLE, Constants.ICON_COLOR.RED, Constants.ICON_COLOR.TEAL};

    //NotificationButtonListener notificationButtonListener = new NotificationButtonListener(this);


    public AudioHelper(Activity activity, final MediaPlayerService mediaPlayerService, boolean afterDestroyed) {
        this.mediaPlayerService = mediaPlayerService;
        this.activity = activity;
        this.context = activity.getApplicationContext();
        vkApi = new VkApi();

        isSongConnected = false;
        isPlaying = false;
        //playerService = new Intent(context, MediaPlayerService.class);

        bottomPlayer = new BottomPlayer(activity);
        playInfo = MainActivity.getPlayInfo();

        player = (RelativeLayout) activity.findViewById(R.id.bottomPlayer);
        bottomPlay = (Button) player.findViewById(R.id.play_bottom);
        canceled = false;

        prefs = context.getSharedPreferences("playInfo", 0);
        artPrefs = context.getSharedPreferences("arts", 0);
        setIsCycled(prefs.getInt("isCycled", 0));
        setIsShuffled(prefs.getBoolean("isShuffled", false));

        if (afterDestroyed && mediaPlayerService.getIsSongConnected()) {
            isConnecting = mediaPlayerService.getIsConnecting();
            isPlaying = mediaPlayerService.getIsPlaying();
            isSongConnected = mediaPlayerService.getIsSongConnected();

            setupActiveView(mediaPlayerService.getAudioItem());
        }

        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int task = intent.getIntExtra(PARAM_TASK, 0);
                switch (task) {
                    case ON_PREPARED_TASK:
                        isPlaying = true;
                        isSongConnected = true;
                        isConnecting = false;

                        if (bottomPlayer != null) {
                            bottomPlayer.unsetProgress();
                            bottomPlayer.setPlaybottom();
                        }
                        vumeter.resume(true);
                        break;
                    case ON_COMPLETION_TASK:
                        //releaseMp3();
                        setupActiveView(mediaPlayerService.item);
                        int nextPos = mediaPlayerService.activePlaylistIndex - mediaPlayerService.parent.getFirstVisiblePosition();
                        if(nextPos < 0 || nextPos >= mediaPlayerService.parent.getChildCount()) {
                            isHidden = true;
                        } else {
                            focused_view = mediaPlayerService.parent.getChildAt(nextPos);
                            isHidden = false;
                        }
                        setupActiveItem(focused_view);
                        setupActiveView(mediaPlayerService.item);
                        break;
                    case ON_PAUSE:
                        stopMp3();
                        break;
                    case ON_PLAY:
                        playMp3();
                        break;
                    case ON_BUFFER:
                        if (bottomPlayer != null)
                            bottomPlayer.setProgress();
                        break;
                    case ON_UNBUFFER:
                        if (bottomPlayer != null)
                            bottomPlayer.unsetProgress();
                        break;
                    case ON_MEDIA_BUTTON_PRESSED:
                        if(isPlaying)
                            stopMp3();
                        else
                            playMp3();
                        break;
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter(BROADCAST_ACTION);
        context.registerReceiver(br, intentFilter);

    }

    public void setConnectionMp3(AudioItem song, ArrayList<AudioItem> playList, View view) {
        //final AudioItem song = (AudioItem) parent.getItemAtPosition(position);
        this.song_item = song;
        //mediaPlayerService.playList = playList;
        //mediaPlayerService.activePlaylistIndex = playList.indexOf(song_item);

        if (isConnecting && playInfo.activeLink.equals(song_item.link) &&
                playInfo.ACTIVE_LIST_MARK == song_item.list_mark)
            return;

        isConnecting = true;

        mediaPlayerService.setConnection(song_item, context);


        setupActiveItem(view);
        setupActiveView(song_item);
        bottomPlayer.setProgress();
        isHidden = false;


    }

    public void setupActiveItem(View view) {
        if (vumeter != null)
            vumeter.setVisibility(View.INVISIBLE);
        if (itemPlay != null)
            itemPlay.setVisibility(View.VISIBLE);


        if(view!=null && !isHidden) {
            this.focused_view = view;

            itemPlay = (ImageView) view.findViewById(R.id.play_button_item);
            vumeter = (VuMeterView) view.findViewById(R.id.vumeter);

            vumeter.setVisibility(View.VISIBLE);
            itemPlay.setVisibility(View.INVISIBLE);
        }
    }

    public void setupActiveView(AudioItem song_item) {
        bottomPlayer.initialize(song_item, this);
        bottomPlayer.setPlaybottom();
        bottomPlayer.setBottomPlayer();

        playInfo.duration = song_item.duration;
        playInfo.activeLink = song_item.link;
        playInfo.ACTIVE_LIST_MARK = song_item.list_mark;
        playInfo.activeName = song_item.name;
        playInfo.id = song_item.id;
        playInfo.ownerId = song_item.ownerId;
        playInfo.activeAuthorName = song_item.author;
        playInfo.currentSong = song_item;
    }


    public View getFocusedView() {
        return focused_view;
    }

    public int isCycled() {
        return mediaPlayerService.isCycled;
    }

    public boolean isShuffled() {
        return mediaPlayerService.isShuffled;
    }

    public void setIsCycled(int flag) {
        mediaPlayerService.isCycled = flag;
        edit = prefs.edit();
        edit.putInt("isCycled", flag);
        edit.apply();
    }

    public void setIsShuffled(boolean flag) {
        mediaPlayerService.isShuffled = flag;
        edit = prefs.edit();
        edit.putBoolean("isShuffled", flag);
        edit.apply();
    }

    public void playMp3() {
        if (isSongConnected && !isPlaying) {
            // deleteAllButtons();
            //createStopBt();
            if (vumeter != null)
                vumeter.resume(true);
            bottomPlay.setBackgroundResource(R.drawable.ic_pause_blue);
            mediaPlayerService.play();
            mediaPlayerService.initNotification();
            isPlaying = true;
        }
    }

    public void stopMp3() {
        if (isPlaying) {
            //deleteAllButtons();
            //createPlayBt();
            if (vumeter != null)
                vumeter.stop(true);
            bottomPlay.setBackgroundResource(R.drawable.ic_play_blue);
            mediaPlayerService.pause();
            mediaPlayerService.initNotification();
            isPlaying = false;
        }
    }

    public void releaseMp3() {
        if (isSongConnected) {
            if (itemPlay != null) {
                itemPlay.setBackgroundResource(R.drawable.disc_main1);
            }
            if(mediaPlayerService!=null)
                mediaPlayerService.release();

            isSongConnected = false;
            isPlaying = false;
            isConnecting = false;

            if (playInfo != null) {
                playInfo.activeLink = "";
                playInfo.ACTIVE_LIST_MARK = -1;
                playInfo.activeName = "";
                playInfo.activeAuthorName = "";
                playInfo.ownerId = -1;
                playInfo.id = -1;
                playInfo.currentSong = null;
                playInfo.duration = 0;
            }

            if(bottomPlayer!=null)
                bottomPlayer.setLayoutVisibility(false);
            //context.unregisterReceiver(br);
        }
    }

    public void seekMp3(int millis) {
        if (isSongConnected)
            mediaPlayerService.seek(millis);
    }

    public void setFocusParams(int pos, View child) {
        focused_position = pos;
        focused_view = child;
    }
    public void setParent(AdapterView<?> parent) {
        mediaPlayerService.parent = parent;
        //mediaPlayerService.playList = ((PlayListAdapter)parent.getAdapter()).getActivePlaylist();
        //mediaPlayerService.activePlaylistIndex = mediaPlayerService.playList.indexOf(song_item);
    }

    public int getMp3Duration() {
        return mediaPlayerService.getDuration();
    }

    public int getMp3Progress() {
        if (isSongConnected)
            return mediaPlayerService.getPosition();
        return -1;
    }

    public void playNext() {
        mediaPlayerService.playNext();

        int nextPos = mediaPlayerService.activePlaylistIndex - mediaPlayerService.parent.getFirstVisiblePosition();
        if(nextPos < 0 || nextPos >= mediaPlayerService.parent.getChildCount()) {
            isHidden = true;
        } else {
            focused_view = mediaPlayerService.parent.getChildAt(nextPos);
            isHidden = false;
        }
        setupActiveItem(focused_view);
        setupActiveView(mediaPlayerService.item);

    }

    public void playPrevious() {
        mediaPlayerService.playPrevious();

        setupActiveView(mediaPlayerService.item);
        int nextPos = mediaPlayerService.activePlaylistIndex - mediaPlayerService.parent.getFirstVisiblePosition();
        if(nextPos < 0 || nextPos >= mediaPlayerService.parent.getChildCount()) {
            isHidden = true;
        } else {
            focused_view = mediaPlayerService.parent.getChildAt(nextPos);
            isHidden = false;
        }
        setupActiveItem(focused_view);
        setupActiveView(mediaPlayerService.item);
    }

    public void stopService() {
        if (mediaPlayerService != null) {
            mediaPlayerService.cancelNotification();
            //mediaPlayerService.clearData();
        }
    }


    public void setActivePlaylist(ArrayList<AudioItem> list) {
        if(mediaPlayerService!=null)
            mediaPlayerService.playList = list;
    }

    public void setActivePlaylistIndex(int index) {
        if(mediaPlayerService!=null)
            mediaPlayerService.activePlaylistIndex = index;
    }

    public void putImagePref(String path, String url) {
        SharedPreferences.Editor edit = artPrefs.edit();
        edit.putString(path, url);
        edit.apply();
    }


    public boolean getIsSongConnected() {
        return mediaPlayerService.isSongConnected;
    }

    public boolean getIsPlaying() {
        return isPlaying;
    }

    public boolean getIsConnecting() {
        return isConnecting;
    }

    public void setButtonHolder(FrameLayout layout, ImageView itemPlay, VuMeterView vumeterView) {
        button_holder = layout;
        vumeter = vumeterView;
        this.itemPlay = itemPlay;
    }

    public void putFavPrefs(String key, String value) {
        SharedPreferences favPrefs = context.getSharedPreferences("favs", 0);
        SharedPreferences.Editor edit = favPrefs.edit();
        edit.putString(key, value);
        edit.apply();
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        releaseMp3();
        playNext();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        if (canceled) {
            mp.release();
            canceled = false;
        } else {
            bottomPlayer.unsetProgress();
            mp.start();
            isPlaying = true;
            isSongConnected = true;
        }
        isConnecting = false;
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
        switch (what) {

            case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                Toast.makeText(context,
                        "NOT_VALID_FOR_PROGRESSIVE_PLAYBACK", Toast.LENGTH_SHORT).show();
                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                Toast.makeText(context,
                        "SERVER_DIED", Toast.LENGTH_SHORT).show();
                break;
            case MediaPlayer.MEDIA_ERROR_TIMED_OUT:
                Toast.makeText(context,
                        "TIMED_OUT", Toast.LENGTH_SHORT).show();
                break;
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                Toast.makeText(context,
                        "ERROR_UNKNOWN", Toast.LENGTH_SHORT).show();
                break;
        }
        return false;
    }

    public ArrayList<AudioItem> loadMp3s(String path) {
        File file = new File(path);
        ArrayList<AudioItem> listmp3 = new ArrayList<>();

        if (file.isDirectory()) {
            File[] files = file.listFiles();

            if (files != null && files.length > 0) {
                String[] splits;

                //Arrays.sort(files, new SortFiles());

                for (int i=0;i<files.length;i++) {
                    if(files[i]==null && files[i].isDirectory())
                        continue;

                    addFromFile(listmp3, files[i]);
                }
            }
        }
        Collections.sort(listmp3, new SortFiles());
        return listmp3;
    }

    public void addFromFile(ArrayList<AudioItem> listmp3, File file) {

        if(!file.canExecute()) {
            return;
        }

       /* if(!file.getName().endsWith(".mp3"))
            return;*/

        AudioItem item = new AudioItem();
        item.link = file.getAbsolutePath();
        item.list_mark = FAVS_LIST_MARK;
        Random rand = new Random();

                   /* if(f.getName().contains("-")) {
                        splits = f.getName().split("-");
                        item.setMp3Name(splits[1]);
                        item.setAuthor(splits[0]);
                    }
                    else
                        item.setMp3Name(f.getName());*/

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(file.getAbsolutePath());


        String artist = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        String title = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
        String duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);

        item.author = artist==null? "unknown artist": artist;
        item.name = title==null ? file.getName(): title;
        item.duration = duration!=null? Integer.parseInt(duration): 0;
        item.picUrl = artPrefs.getString(file.getAbsolutePath(), "");
        item.lastModified = file.lastModified();
        //item.back = iconBacks[rand.nextInt(5)];

        listmp3.add(item);
    }

    public ArrayList<AudioItem> getFavsList(SharedPreferences prefs) {
        ArrayList<AudioItem> items = new ArrayList<>();
        AudioItem item;
        ArrayList<File> files = new ArrayList<>();

        Map<String, ?> prefs_values = prefs.getAll();
        if(prefs_values!=null)
            for(Map.Entry<String, ?> entry: prefs_values.entrySet()) {
                if(Integer.parseInt(entry.getValue().toString()) !=-1) {
                    File file = new File(entry.getKey());
                    //files.add(file);

                    if(file.exists() && !file.isDirectory())
                        files.add(file);//addFromFile(items, file);
                }
            }
            //Collections.sort(files, new SortFiles());

            for(int i=0;i<files.size();i++) {
                addFromFile(items, files.get(i));
            }
        //bottomPlayer.setFavsListLength(items.size());
        Collections.sort(items, new SortFiles());
        return items;
    }

  /*  public class SortFiles implements Comparator<File> {
        @Override
        public int compare(File f1, File f2) {
            return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());
        }
    }
*/
    public class SortFiles implements Comparator<AudioItem> {
        @Override
        public int compare(AudioItem f1, AudioItem f2) {
            return Long.valueOf(f2.lastModified).compareTo(f1.lastModified);
        }
    }

   /* public class SortPlaylists implements Comparator<Map.Entry<String, ?>> {
        @Override
        public int compare(Map.Entry<String, ?> f1, Map.Entry<String, ?> f2) {
            return Long.valueOf(Long.parseLong(f2.getValue().toString())).compareTo(Long.parseLong(f1.getValue().toString()));
        }
    }*/

    public ArrayList<AudioItem> searchInLoads(String query) {
        ArrayList<AudioItem> loads = loadMp3s(MainActivity.path);
        ArrayList<AudioItem> result = new ArrayList<>();

        for(int i=0;i<loads.size();i++) {
            if(loads.get(i).name.toLowerCase().contains(query.toLowerCase()))
                result.add(loads.get(i));
        }
        return result;
    }

    public ArrayList<AudioItem> getPlaylists() {
        ArrayList<AudioItem> items = new ArrayList<>();
        AudioItem item;
        SharedPreferences playlistPrefs = context.getSharedPreferences("playListsSize",0);
        SharedPreferences playlistTime = context.getSharedPreferences("playLists2",0);

        Map<String, ?> prefs_values = playlistPrefs.getAll();
        if(prefs_values!=null)
            for(Map.Entry<String, ?> entry: prefs_values.entrySet()) {
                if(Long.parseLong(entry.getValue().toString())!=-1) {
                    item = new AudioItem();
                    item.author = entry.getValue().toString() +" items";
                    item.name = entry.getKey();
                    item.list_mark = FAVS_LIST_MARK;
                    item.link = "none";
                    item.isPlaylist=true;
                    item.lastModified = playlistTime.getLong(item.name, 0);
                    //item.back = iconBacks[rand.nextInt(5)];
                    items.add(item);
                }
            }
        //bottomPlayer.setFavsListLength(items.size());
        Collections.sort(items, new SortFiles());
        return items;
    }


}
