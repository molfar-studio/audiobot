package com.crossfeed.favdoc;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ivan on 26.08.17.
 */

public class LastFmApi extends Api {
    private String domain = "http://ws.audioscrobbler.com/2.0/?";
    private String apiKey = "1cb43d2f1573fd8648c283eab1a532a0";

    public String getArtUrlRequest(String query) {
        query = query.replaceAll(" ", "_");
        StringBuilder sb = new StringBuilder();
        sb.append(domain).append("method=track.search&")
                .append("track=").append(query).append("&api_key=")
                .append(apiKey).append("&format=json");
        return  sb.toString();
    }

    public String getArtUrl(String request) {
        String content = getResponseText(request);
        String artUrl = null;

        try {
            JSONObject root = new JSONObject(content);
            root = root.getJSONObject("results");
            root = root.getJSONObject("trackmatches");
            JSONArray tracks = root.getJSONArray("track");

            if(tracks.length()!=0) {
                JSONArray images = tracks.getJSONObject(0).getJSONArray("image");
                artUrl = !images.isNull(1)? images.getJSONObject(1).getString("#text"): null;
            }
        }
        catch (JSONException e) {}
        return artUrl;
    }
}












































