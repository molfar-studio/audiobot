package com.crossfeed.favdoc;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import java.util.Locale;

/**
 * Created by ivan on 26.08.17.
 */

public class UserPrefs {

    private static String getUserCountryPref(Context context) {
        SharedPreferences countryPrefs = context.getSharedPreferences("userCountry",0);
        return countryPrefs.getString("countryCode", null);
    }

    public static String getUserCountry(Context context) {
        if(getUserCountryPref(context)==null) {
            try {
                SharedPreferences countryPrefs = context.getSharedPreferences("userCountry",0);
                SharedPreferences.Editor edit;
                final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                final String simCountry = tm.getSimCountryIso();

                if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                    edit = countryPrefs.edit();
                    edit.putString("countryCode", simCountry.toLowerCase(Locale.US));
                    edit.apply();
                    return simCountry.toLowerCase(Locale.US);

                } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                    String networkCountry = tm.getNetworkCountryIso();
                    if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                        edit = countryPrefs.edit();
                        edit.putString("countryCode", networkCountry.toLowerCase(Locale.US));
                        edit.apply();
                        return networkCountry.toLowerCase(Locale.US);
                    }
                }
            } catch (Exception e) {
                Toast.makeText(context,
                        "exception getting countryCode", Toast.LENGTH_SHORT).show();

            }
        }
        else
            return getUserCountryPref(context);
        return null;
    }

}
