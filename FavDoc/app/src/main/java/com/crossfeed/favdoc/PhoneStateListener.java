package com.crossfeed.favdoc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

/**
 * Created by ivan on 07.09.17.
 */

public class PhoneStateListener extends BroadcastReceiver {

    public void onReceive(Context context, Intent intent) {
        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

        if(state.equals(TelephonyManager.EXTRA_STATE_RINGING)){
            Intent pauseIntent = new Intent(AudioHelper.BROADCAST_ACTION);
            pauseIntent.putExtra(AudioHelper.PARAM_TASK, AudioHelper.ON_PAUSE);
            context.sendBroadcast(pauseIntent);
        }

    }
}
