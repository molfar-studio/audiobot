package com.crossfeed.favdoc;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by hawk on 08.01.2017.
 */
   class ViewPagerAdapter extends FragmentPagerAdapter {
    private final int COUNT = 2;
    private final String[] tab_names= {"Search", "Favs", "Saved"};

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                return new SearchFragment();
            default:
                return new FavsFragment();
        }
    }

    @Override
    public int getCount() {
        return COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch(position) {
            case 0:
                return tab_names[0];
            default:
                return tab_names[1];
        }
    }
}
