package com.crossfeed.favdoc;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.media.session.MediaButtonReceiver;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RemoteViews;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by ivan on 22.07.17.
 */

public class MediaPlayerService extends Service implements MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener {

    MediaPlayer mediaPlayer;
    MyBinder binder = new MyBinder();
    boolean isCanceled = false;
    public static final int NOTIFICATION_ID = 1;
    AudioItem item;
    boolean isSongConnected = false;
    boolean isConnecting = false;
    boolean destroyedMain = false;
    Context context;
    CurrentPlayInfo playInfo;

    NotificationManager notificationManager;

    TelephonyManager telephonyManager;
    PhoneStateListener phoneStateListener;
    boolean isPausedInCall = false;
    int headsetSwitch = 1;
    BroadcastReceiver headsetReceiver;
    MediaButtonReceiver mediaButtonReceiver;
    public ArrayList<AudioItem> playList;
    public AdapterView<?> parent;
    public int activePlaylistIndex;
    public int isCycled = 0;
    public boolean isShuffled = false;

    @Override
    public void onCreate() {
        super.onCreate();

        mediaPlayer = new MediaPlayer();

        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnErrorListener(this);

        headsetReceiver = new BroadcastReceiver() {
            boolean headsetConnected = false;

            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("state")) {
                    if (headsetConnected && intent.getIntExtra("state", 0) == 0) {
                        headsetConnected = false;
                        headsetSwitch = 0;
                    } else if (!headsetConnected && intent.getIntExtra("state", 0) == 1) {
                        headsetConnected = true;
                        headsetSwitch = 1;
                    }
                }

                switch (headsetSwitch) {
                    case 0:
                        headsetDisconnected();
                        break;
                    case 1:
                        break;
                }
            }
        };

        mediaButtonReceiver = new MediaButtonReceiver();

        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        phoneStateListener = new PhoneStateListener() {

            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                super.onCallStateChanged(state, incomingNumber);

                switch (state) {
                    //case TelephonyManager.CALL_STATE_OFFHOOK:
                    case TelephonyManager.CALL_STATE_RINGING:
                        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                            mediaPlayer.pause();
                            sendPlayPauseBroadcast(false);
                            //isPausedInCall = true;
                        }
                        break;
                    /*case TelephonyManager.CALL_STATE_IDLE:
                        if (mediaPlayer != null && isPausedInCall) {
                            sendPlayPauseBroadcast(true);
                            isPausedInCall = false;
                        }*/
                }
            }
        };
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    private void headsetDisconnected() {
        sendPlayPauseBroadcast(false);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);

        registerReceiver(headsetReceiver, new IntentFilter(Intent.ACTION_HEADSET_PLUG));

        //IntentFilter mediaButtonFilter = new IntentFilter(Intent.ACTION_MEDIA_BUTTON);
        //mediaButtonFilter.setPriority(1000);
        //registerReceiver(mediaButtonReceiver, mediaButtonFilter);
        playInfo = MainActivity.getPlayInfo();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            mediaPlayer.release();
            isSongConnected = false;
        }
        cancelNotification();

        if (telephonyManager != null) {
            //telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        }
    }

    public void setCanceled(boolean flag) {
        isCanceled = flag;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        if (mediaPlayer != null) {
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnErrorListener(this);
        }

        return binder;
    }

    public void clearData() {
        mediaPlayer = null;
    }

    public void setConnection(AudioItem item, Context context) {
        this.item = item;
        Uri song_uri = Uri.parse(item.link);

        try {
            mediaPlayer = new MediaPlayer();
            //mediaPlayer.reset();
            this.context = context;

            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnErrorListener(this);

            //initNotification();

            mediaPlayer.setDataSource(context, song_uri);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            isConnecting = true;
            mediaPlayer.prepareAsync();

        } catch (IOException e) {
            Toast.makeText(context, "IOException", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(context, "Exception prepare", Toast.LENGTH_SHORT).show();
        }
    }

    public void release() {
        if (mediaPlayer != null)
            mediaPlayer.release();

        isSongConnected = false;
        isConnecting = false;

    }

    public boolean getIsPlaying() {
            return mediaPlayer != null && mediaPlayer.isPlaying();
    }

    public boolean getIsConnecting() {
        return isConnecting;
    }

    public boolean getIsSongConnected() {
        return isSongConnected;
    }

    public AudioItem getAudioItem() {
        return item;
    }

    private RemoteViews createNotificationView() {
        RemoteViews view = new RemoteViews("com.crossfeed.favdoc", R.layout.statusbar_player);
        //view.setImageViewResource(R.id.ntf_imageview, [SONG IMAGE] );
        view.setTextViewText(R.id.ntf_title, (item.name.length() > 12 ? item.name.substring(0, 12) + "..." : item.name));
        view.setTextViewText(R.id.ntf_subtitle, (item.author.length() > 14 ? item.author.substring(0, 14) + "..." : item.author));
        view.setOnClickPendingIntent(R.id.ntf_btn_close, getClosePendingIntent());
        view.setOnClickPendingIntent(R.id.ntf_btn_next, getPlayNextPendingIntent());
        view.setOnClickPendingIntent(R.id.ntf_btn_prev, getPlayPreviousPendingIntent());

        if (getIsPlaying()) {
            view.setViewVisibility(R.id.ntf_btn_play, View.GONE);
            view.setViewVisibility(R.id.ntf_btn_pause, View.VISIBLE);
            view.setOnClickPendingIntent(R.id.ntf_btn_pause, getPausePendingIntent());
        } else {
            view.setViewVisibility(R.id.ntf_btn_play, View.VISIBLE);
            view.setViewVisibility(R.id.ntf_btn_pause, View.GONE);
            view.setOnClickPendingIntent(R.id.ntf_btn_play, getPlayPendingIntent());
        }

        return view;
    }

    private RemoteViews createBigNotificationView() {
        RemoteViews view = new RemoteViews("com.crossfeed.favdoc", R.layout.statusbar_player_big);
        //view.setImageViewResource(R.id.bntf_imageview, [SONG IMAGE] );
        view.setTextViewText(R.id.bntf_title, (item.name.length() > 35 ? item.name.substring(0, 35) + "..." : item.name));
        view.setTextViewText(R.id.bntf_subtitle, (item.author.length() > 40 ? item.author.substring(0, 40) + "..." : item.author));
        view.setOnClickPendingIntent(R.id.bntf_btn_close, getClosePendingIntent());
        view.setOnClickPendingIntent(R.id.bntf_btn_next, getPlayNextPendingIntent());
        view.setOnClickPendingIntent(R.id.bntf_btn_prev, getPlayPreviousPendingIntent());

        if (getIsPlaying()) {
            view.setViewVisibility(R.id.bntf_btn_play, View.GONE);
            view.setViewVisibility(R.id.bntf_btn_pause, View.VISIBLE);
            view.setOnClickPendingIntent(R.id.bntf_btn_pause, getPausePendingIntent());
        } else {
            view.setViewVisibility(R.id.bntf_btn_play, View.VISIBLE);
            view.setViewVisibility(R.id.bntf_btn_pause, View.GONE);
            view.setOnClickPendingIntent(R.id.bntf_btn_play, getPlayPendingIntent());
        }

        return view;
    }


    public void initNotification() {

        Intent resultIntent = new Intent(this, MainActivity.class).putExtra("audioitem", item);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_play_circle_filled_blue_24dp)
                        .setOngoing(true)
                        .setContent(createNotificationView())
                        .setCustomBigContentView(createBigNotificationView())
                        .setContentIntent(resultPendingIntent);
        startForeground(NOTIFICATION_ID, mBuilder.build());
    }

    public void cancelNotification() {
        stopForeground(true);
    }

    private PendingIntent getClosePendingIntent() {
        Intent intent = new Intent("ntf_button");
        intent.putExtra("action", "close");
        return PendingIntent.getBroadcast(getApplicationContext(), 1000, intent, 0);
    }

    private PendingIntent getPlayPendingIntent() {
        Intent intent = new Intent("ntf_button");
        intent.putExtra("action", "play");
        return PendingIntent.getBroadcast(getApplicationContext(), 1001, intent, 0);
    }

    private PendingIntent getPausePendingIntent() {
        Intent intent = new Intent("ntf_button");
        intent.putExtra("action", "pause");
        return PendingIntent.getBroadcast(getApplicationContext(), 1002, intent, 0);
    }

    private PendingIntent getPlayNextPendingIntent() {
        Intent intent = new Intent("ntf_button");
        intent.putExtra("action", "next");
        return PendingIntent.getBroadcast(getApplicationContext(), 1003, intent, 0);
    }

    private PendingIntent getPlayPreviousPendingIntent() {
        Intent intent = new Intent("ntf_button");
        intent.putExtra("action", "previous");
        return PendingIntent.getBroadcast(getApplicationContext(), 1004, intent, 0);
    }


    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        if (mediaPlayer != null) {
            //cancelNotification();
            mediaPlayer.release();

            if(isCycled == 0) {
                activePlaylistIndex = playList.size() > activePlaylistIndex + 1 &&
                        !playList.get(activePlaylistIndex+1).isPlaylist ? ++activePlaylistIndex : activePlaylistIndex;
            }
            else if(isCycled==1) {
                do {
                    activePlaylistIndex = playList.size() > activePlaylistIndex + 1 ? ++activePlaylistIndex : 0;
                }
                while (playList.get(activePlaylistIndex).isPlaylist);
            }

            setConnection(playList.get(activePlaylistIndex), context);

            Intent intent = new Intent(AudioHelper.BROADCAST_ACTION);
            intent.putExtra(AudioHelper.PARAM_TASK, AudioHelper.ON_COMPLETION_TASK);
            sendBroadcast(intent);
        }
    }

    public void playNext() {
        mediaPlayer.release();
        Random rand = new Random();
        int nextRand;

       if(isShuffled) {
            while ((nextRand = rand.nextInt(playList.size() - 1)) == activePlaylistIndex) {}
                activePlaylistIndex = nextRand;
        }
        else if(isCycled == 0) {
            activePlaylistIndex = playList.size() > activePlaylistIndex + 1 &&
                    !playList.get(activePlaylistIndex+1).isPlaylist ? ++activePlaylistIndex : activePlaylistIndex;
        }
        else {
            do {
                activePlaylistIndex = playList.size() > activePlaylistIndex + 1 ? ++activePlaylistIndex : 0;
            }
            while (playList.get(activePlaylistIndex).isPlaylist);
        }


        setConnection(playList.get(activePlaylistIndex), context);
    }

    public void playPrevious() {
        mediaPlayer.release();
        Random rand = new Random();
        int nextRand;

        if(isShuffled) {
            while ((nextRand = rand.nextInt(playList.size() - 1)) == activePlaylistIndex) {}
            activePlaylistIndex = nextRand;
        }
        if(isCycled == 0)
            activePlaylistIndex = 0 <= activePlaylistIndex-1 &&
                    !playList.get(activePlaylistIndex-1).isPlaylist? --activePlaylistIndex: activePlaylistIndex;
        else {
            do {
                activePlaylistIndex = 0 <= activePlaylistIndex - 1 ? --activePlaylistIndex : playList.size() - 1;
            }
            while (playList.get(activePlaylistIndex).isPlaylist);
        }


        setConnection(playList.get(activePlaylistIndex), context);
    }


    @Override
    public void onPrepared(MediaPlayer mp) {
        if (mediaPlayer != mp) {
            mp.release();
            //isCanceled = false;
        } else {
            mp.start();
            isSongConnected = true;
            isConnecting = false;

            initNotification();

            Intent intent = new Intent(AudioHelper.BROADCAST_ACTION);
            intent.putExtra(AudioHelper.PARAM_TASK, AudioHelper.ON_PREPARED_TASK);
            sendBroadcast(intent);
        }
    }


    @Override
    public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
        switch (what) {

            case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                Toast.makeText(getApplicationContext(),
                        "NOT_VALID_FOR_PROGRESSIVE_PLAYBACK", Toast.LENGTH_SHORT).show();
                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                Toast.makeText(getApplicationContext(),
                        "SERVER_DIED", Toast.LENGTH_SHORT).show();
                break;
            case MediaPlayer.MEDIA_ERROR_TIMED_OUT:
                Toast.makeText(getApplicationContext(),
                        "TIMED_OUT", Toast.LENGTH_SHORT).show();
                break;
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                Toast.makeText(getApplicationContext(),
                        "ERROR_UNKNOWN", Toast.LENGTH_SHORT).show();
                break;
        }
        return false;
    }

    public void play() {
        if (mediaPlayer != null)
            mediaPlayer.start();
    }

    public void pause() {
        if (mediaPlayer != null)
            mediaPlayer.pause();
    }

    public void seek(int ms) {
        if (mediaPlayer != null)
            mediaPlayer.seekTo(ms);
    }

    public int getPosition() {
        if (mediaPlayer != null && isSongConnected)
            return mediaPlayer.getCurrentPosition();
        return 0;
    }

    public int getDuration() {
        if (mediaPlayer != null)
            return mediaPlayer.getDuration();
        return 0;
    }

    @Override
    public boolean onInfo(MediaPlayer mediaPlayer, int what, int extra) {
        switch (what) {
            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                sendBufferBroadcast(true);
                break;
            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                sendBufferBroadcast(false);
                break;
        }
        return false;
    }

    class MyBinder extends Binder {
        MediaPlayerService getService() {
            return MediaPlayerService.this;
        }
    }

    private void sendPlayPauseBroadcast(boolean flag) {
        Intent intent = new Intent(AudioHelper.BROADCAST_ACTION);
        if (flag)
            intent.putExtra(AudioHelper.PARAM_TASK, AudioHelper.ON_PLAY);
        else
            intent.putExtra(AudioHelper.PARAM_TASK, AudioHelper.ON_PAUSE);
        sendBroadcast(intent);
    }

    private void sendBufferBroadcast(boolean flag) {
        Intent intent = new Intent(AudioHelper.BROADCAST_ACTION);
        if (flag)
            intent.putExtra(AudioHelper.PARAM_TASK, AudioHelper.ON_BUFFER);
        else
            intent.putExtra(AudioHelper.PARAM_TASK, AudioHelper.ON_UNBUFFER);
        sendBroadcast(intent);
    }
}
