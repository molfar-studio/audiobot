package com.crossfeed.favdoc;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by ivan on 22.07.17.
 */

    public class Constants {
        public interface ACTION {
            public static String MAIN_ACTION = "com.marothiatechs.customnotification.action.main";
            public static String INIT_ACTION = "com.marothiatechs.customnotification.action.init";
            public static String PREV_ACTION = "com.marothiatechs.customnotification.action.prev";
            public static String PLAY_ACTION = "com.marothiatechs.customnotification.action.play";
            public static String NEXT_ACTION = "com.marothiatechs.customnotification.action.next";
            public static String STARTFOREGROUND_ACTION = "com.marothiatechs.customnotification.action.startforeground";
            public static String STOPFOREGROUND_ACTION = "com.marothiatechs.customnotification.action.stopforeground";

        }

    public interface ICON_COLOR {
        public static int PINK = R.drawable.circle_img_pink;
        public static int PURPLE = R.drawable.circle_img_purple;
        public static int DEEP_PURPLE= R.drawable.circle_img_deep_purple;
        public static int TEAL = R.drawable.circle_img_teal;
        public static int RED = R.drawable.circle_img_red;


    }


        public interface NOTIFICATION_ID {
            public static int FOREGROUND_SERVICE = 101;
        }

        public static Bitmap getDefaultAlbumArt(Context context) {
            Bitmap bm = null;
            BitmapFactory.Options options = new BitmapFactory.Options();
            try {
                bm = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.icon, options);
            } catch (Error ee) {
            } catch (Exception e) {
            }
            return bm;
        }

    }
