package com.crossfeed.favdoc;

import android.content.Context;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.regex.Matcher;

/**
 * Created by hawk on 04.01.2017.
 */
public class Searcher {

    Matcher matcher;
    final String query_mandatory = " скачать";
    String[] level2cutWords =  {"скачать песню","слушать онлайн", "скачать песню в mp3", "слушать музыку онлайн"};
    String[] level1cutWords = {"скачать", "бесплатно", "mp3", "музыку в mp3"};
    final int SEARCH_LIST_MARK=0;
    BrowserInfo browserInfo;
    Context context;

    public Searcher(BrowserInfo browserInfo, Context context) {
        this.browserInfo = browserInfo;
        this.context = context;
    }

    public ArrayList<String> searchLinksByGoogle(String query, String request_link) {
        query = validateQuery(query);
        String request = request_link + query+"&num=20";
        ArrayList<String> search_result = new ArrayList<>();
        try {
            Document doc = Jsoup
                    .connect(request)
                    .userAgent(browserInfo.getUserAgent())
                    .timeout(0)
                    .execute().parse();


            Elements links = doc.select("a[href]");

            for (Element link : links) {
                String temp = link.attr("href");

                if (temp.startsWith(browserInfo.getGoogleLinkMarker())) {
                    temp = temp.replace(browserInfo.getGoogleLinkMarker(), "");

                    if(!(temp.toLowerCase().contains(browserInfo.getCacheMarker()))) {
                        if (temp.length()<200 && temp.startsWith("http"))
                            search_result.add(temp.split("&sa")[0]);
                    }
                }
            }

        } catch (IOException e) {
            Toast.makeText(context,
                    "searchLinksByGoogle() threw exception", Toast.LENGTH_SHORT).show();
        }
        return search_result;
    }

    public ArrayList<AudioItem> getLightScanMp3Links(ArrayList<String> urls) {
        ArrayList<AudioItem> result = new ArrayList<>();
        try {
            for(int page_count=0; page_count<urls.size(); page_count++) {
                String prev_domain = "";

                if(page_count>0) {
                    prev_domain = getDomainName(urls.get(page_count - 1));
                    if (!prev_domain.equals("") && urls.get(page_count).contains(prev_domain))
                        continue;
                }

                if (!CurrentPlayInfo.isNetworkConnected(context)) {
                    Toast.makeText(context,
                            R.string.network_access_error_text, Toast.LENGTH_SHORT).show();
                    break;
                }

                Document page = Jsoup
                            .connect(urls.get(page_count))
                            .userAgent(browserInfo.getUserAgent())
                            .ignoreHttpErrors(true)
                            .followRedirects(true)
                            .timeout(0).execute().parse();

                if (page != null) {
                    Elements mp3_links = page.select("a[href$=.mp3]");
                    if (mp3_links.size() == 0)
                        mp3_links = page.select("div[href$=.mp3]");

                    for (int link_count = 0; link_count < mp3_links.size(); link_count++) {
                        AudioItem song_item = new AudioItem();
                        String link_string = mp3_links.get(link_count).attr("href");
                        String title = page.title();
                        title = validateTitle(title);
                       /* song_item.setMp3Name(title);
                        song_item.setListMark(SEARCH_LIST_MARK);
                        link_count++;

                        if (!link_string.startsWith("http"))
                            song_item.setLink(getDomainName(urls.get(0)) + link_string);
                        else
                            song_item.setLink(link_string);
*/
                        result.add(song_item);

                        if (link_count > 4)
                            break;
                    }
                    /*if(mp3_links.size()!=0) {
                        AudioItem song_item = new AudioItem();
                        String link = mp3_links.get(0).attr("href");
                        String title = page.title();
                        title = validateTitle(title);

                        song_item.setMp3Name(title);
                        song_item.setListMark(SEARCH_LIST_MARK);
                        song_item.setPageSource(urls.get(page_count));

                        if (!link.startsWith("http"))
                            song_item.setLink(getDomainName(urls.get(0)) + link);
                        else
                            song_item.setLink(link);

                        result.add(song_item);
                    }*/

                }

                if(result.size()>0)
                    break;
                if(page_count>5)
                    break;
              }
            }

        catch (UnknownHostException e) {
            Toast.makeText(context,
                    "UnknownHost threw exception", Toast.LENGTH_SHORT).show();
        }
        catch(IOException e) {
            Toast.makeText(context,
                    "IO getScan...() threw exception", Toast.LENGTH_SHORT).show();
        }
        catch(Exception e) {
            Toast.makeText(context,
                    "Exception getScan...() threw exception", Toast.LENGTH_SHORT).show();
        }

        return result;

    }

    public String getDomainName(String url) {
        String domain = "";
        matcher = browserInfo.getDomainPatternName().matcher(url);
        if(matcher.find())
            domain = matcher.group(0).toLowerCase().trim();
        return domain;
    }

    public String validateQuery(String query) {
        if(!(query.toLowerCase().contains(query_mandatory)))
            query+=" "+query_mandatory;
        return query;
    }

    public String validateTitle(String name) {
        String title = name;
        title = title.toLowerCase();

        for(String word: level2cutWords) {
            if(title.contains(word))
                title = title.replace(word, "");
        }
        for(String word: level1cutWords) {
            if(title.contains(word))
                title = title.replace(word, "");
        }
        if(title.length()>55)
            title = title.substring(0,55)+"...";

        title = title.trim();
        return title;
    }
}
