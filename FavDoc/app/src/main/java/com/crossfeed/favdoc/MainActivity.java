package com.crossfeed.favdoc;


import android.app.Activity;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import static android.os.Environment.DIRECTORY_DOWNLOADS;
import static android.os.Environment.DIRECTORY_MUSIC;
import static com.crossfeed.favdoc.R.id.clear_text_bt;


public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener, MenuItemCompat.OnActionExpandListener,
        NavigationView.OnNavigationItemSelectedListener,
        AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, AbsListView.OnScrollListener {

    SharedPreferences prefs;
    SharedPreferences playlistPrefs;
    SharedPreferences.Editor edit;
    ViewPager viewPager;
    TabLayout tabs;

    static onStreamPlaylistClickListener clickListener;
    private static CurrentPlayInfo playInfo;

    ServiceConnection sConn;
    MediaPlayerService mediaPlayerService;
    boolean bound = false;
    private Intent playerService;
    AudioHelper audioHelper;
    Activity act;
    Context context;
    //ViewPagerAdapter adapter;
    ListView favs_list;
    PlayListAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    //private SwipeRefreshLayout emptyRefreshLayout;
    static final int FAVS_LIST_MARK = 1;
    Searcher searcher;
    ProgressBar progressBar;
    //String search_request = "https://www.google.ru/search?q=";
    VkApi vkApi;
    public static String path;
    FloatingActionButton fab;
    ActionMode actionMode;
    LinearLayout emptyView;
    boolean searchMode;
    Random rand;
    boolean isUA = false;
    AsyncTask<String, Void, String> searchingArt;
    static int beginSongPos = 0;
    int mLastFirstVisibleItem = 0;
    boolean goingUp = false;
    ViewGroup headerList;
    int searchOffset = 0;
    String query;
    ArrayList<AudioItem> inLoads;
    boolean isLazyLoading = false;
    View searchFooter;
    public static int theme_code;
    public static boolean settings_changed = false;
    SharedPreferences playlistSize;
    SharedPreferences.Editor playlistSizeEdit;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        SharedPreferences theme_preference = getSharedPreferences("theme", 0);
        theme_code = theme_preference.getInt("theme_code", 0);
        switch (theme_code) {
            case 0: // Default
                break;
            case 1: // Dark
                setTheme(R.style.AppTheme_Dark);

        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        act = this;
        context = getApplicationContext();
        vkApi = new VkApi();
        playInfo = new CurrentPlayInfo(context);
        rand = new Random();

        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        ComponentName receiverComponent = new ComponentName(this, RemoteControlReceiver.class);
        audioManager.registerMediaButtonEventReceiver(receiverComponent);

        IntentFilter filter = new IntentFilter(Intent.ACTION_MEDIA_BUTTON);
        RemoteControlReceiver r = new RemoteControlReceiver();
        registerReceiver(r, filter);

        IntentFilter filterPhone = new IntentFilter(Intent.ACTION_MEDIA_BUTTON);
        PhoneStateListener rPhone = new PhoneStateListener();
        registerReceiver(rPhone, filterPhone);

        //String locale = context.getResources().getConfiguration().locale.getDisplayCountry();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sharedPreferences.getBoolean("alt_pics_source", false)) {
            isUA = true;
            searchingArt = new AsyncTask<String,
                    Void, String>() {
                @Override
                protected void onPreExecute() {
                }

                @Override
                protected String doInBackground(String... param) {
                    LastFmApi lastFmApi = new LastFmApi();
                    return lastFmApi.getArtUrl(lastFmApi.getArtUrlRequest(param[0]));
                }

                @Override
                protected void onPostExecute(String result) {
                    adapter.lastFmArtUrl = result;
                    adapter.notifyDataSetChanged();
                    adapter.notifyDataSetInvalidated();
                }
            };
        }


       /* CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/ubuntu.regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
*/
        /*if(playInfo.isExternalStorageWritable())
            path = Environment.getExternalStorageDirectory().getPath()+"/audiobot";
        else*/
        //path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
       /* if(playInfo.isExternalStorageWritable() &&
                getExternalFilesDir(DIRECTORY_MUSIC)!=null)
            path = getExternalFilesDir(DIRECTORY_MUSIC).getAbsolutePath();*/

        if (playInfo.isExternalStorageWritable()) {
            SharedPreferences sPref = getSharedPreferences("path", 0);
            path = sPref.getString("storage_path_string", getExternalFilesDir(DIRECTORY_MUSIC).getAbsolutePath());
        }


        searchMode = false;

        File file = new File(getExternalFilesDir(DIRECTORY_DOWNLOADS), "test");
        /*try {
            file.createNewFile();
        }
        catch (IOException e) {

        }*/
        Calendar cal = Calendar.getInstance();
        long modDate = cal.getTimeInMillis();

        if (file.setLastModified(modDate))
            Toast.makeText(context,
                    "" + file.lastModified(), Toast.LENGTH_SHORT).show();

        BrowserInfo browserInfo = new BrowserInfo();
        searcher = new Searcher(browserInfo, context);

        progressBar = (ProgressBar) findViewById(R.id.main_progressbar);
        progressBar.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.blue), PorterDuff.Mode.MULTIPLY);

        //prefs = context.getSharedPreferences("favourites", 0);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        favs_list = (ListView) findViewById(R.id.playlist_favs);
        // favs_list.setOnItemLongClickListener(this);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.favs_refresh_layout);
        //emptyRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.empty_refresh_layout);
        emptyView = (LinearLayout) findViewById(R.id.empty_favs);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

        prefs = context.getSharedPreferences("favs", 0);
        playlistPrefs = context.getSharedPreferences("playLists2", 0);
        playlistSize = context.getSharedPreferences("playListsSize", 0);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.CircleStartColor);
        //emptyRefreshLayout.setOnRefreshListener(this);
        //emptyRefreshLayout.setColorSchemeResources(R.color.CircleStartColor);

        playerService = new Intent(context, MediaPlayerService.class);

        sConn = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                bound = true;
                mediaPlayerService = ((MediaPlayerService.MyBinder) iBinder).getService();
                audioHelper = new AudioHelper(act, mediaPlayerService, true);
                clickListener = new onStreamPlaylistClickListener(audioHelper, mediaPlayerService);


                if (path != null)
                    adapter = new PlayListAdapter(getItems(path), context);
                //adapter.playlist = audioHelper.loadMp3s(path);
                favs_list.setAdapter(adapter);

                favs_list.setEmptyView(emptyView);

                favs_list.setOnScrollListener(MainActivity.this);

                if (clickListener != null)
                    favs_list.setOnItemClickListener(MainActivity.this);
                favs_list.setOnItemLongClickListener(MainActivity.this);

                audioHelper.setParent(favs_list);
                //audioHelper.setActivePlaylist(adapter.getActivePlaylist());
                //audioHelper.set0Active();

            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                bound = false;
            }
        };

        startService(playerService);
        bindService(playerService, sConn, Context.BIND_AUTO_CREATE);
    }

    ArrayList<AudioItem> getItems(String path) {
        ArrayList<AudioItem> items = audioHelper.loadMp3s(path);
        ArrayList<AudioItem> playlists = audioHelper.getPlaylists();

        beginSongPos = playlists.size();

        for (int i = 0; i < playlists.size(); i++) {
            items.add(0, playlists.get(i));
        }
        return items;
    }

    @Override
    protected void onStart() {
        super.onStart();

        createDirIfNotExists("audiobot");
        if (adapter != null && favs_list != null)
            favs_list.refreshDrawableState();

        //audioHelper.setupActiveView(playInfo.get);
        //adapter = new PlayListAdapter(getFavsList(), context);
        //favs_list.setAdapter(adapter);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (settings_changed) {
            settings_changed = false;
            //recreate();

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bound)
            unbindService(sConn);
    }


    void updateList() {
        adapter = new PlayListAdapter(getItems(path), this);
        //adapter.playlist = audioHelper.loadMp3s(path);
        favs_list.setAdapter(adapter);
        searchMode = false;
    }

    public boolean createDirIfNotExists(String path) {
        boolean ret = true;

        File file = new File(Environment.getExternalStorageDirectory(), path);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                Log.e("TravellerLog :: ", "Problem creating Image folder");
                ret = false;
            }
        }
        return ret;
    }

    @Override
    public void onRefresh() {
        if (!searchMode)
            updateList();
        swipeRefreshLayout.setRefreshing(false);
        //emptyRefreshLayout.setRefreshing(false);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
            moveTaskToBack(true);

        return super.onKeyDown(keyCode, event);
    }


    public static synchronized onStreamPlaylistClickListener getClickListener() {
        return clickListener;
    }

    public static synchronized CurrentPlayInfo getPlayInfo() {
        return playInfo;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case clear_text_bt:

                break;
            case R.id.fab:
                createPlaylistPopup();
                break;

        }
    }

    public void createPlaylistPopup() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getResources().getString(R.string.popup_new));
        dialog.setIcon(R.drawable.ic_playlist_add_check_black_24dp);

        final EditText input = new EditText(MainActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.requestFocus();
        input.setHint(getResources().getString(R.string.popup_name));
        dialog.setView(input);

        dialog.setPositiveButton(getResources().getString(R.string.popup_btn_create), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (input.getText().toString().equals(""))
                    dialog.cancel();
                edit = playlistPrefs.edit();
                playlistSizeEdit = playlistSize.edit();

                edit.putLong(input.getText().toString(), System.currentTimeMillis());
                playlistSizeEdit.putInt(input.getText().toString(), 0);

                edit.apply();
                playlistSizeEdit.apply();
                dialog.cancel();
                updateList();
            }
        });
        dialog.setNegativeButton(getResources().getString(R.string.popup_btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        dialog.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem searchMenuItem = menu.findItem(R.id.search_btn);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) searchMenuItem.getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint(getResources().getString(R.string.edit_text_hint));

        MenuItemCompat.setOnActionExpandListener(searchMenuItem, this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.down_btn) {
            if (favs_list != null)
                favs_list.setSelection(beginSongPos);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            query = intent.getStringExtra(SearchManager.QUERY);
            fab.setVisibility(View.GONE);
            searchFooter = getLayoutInflater().inflate(R.layout.footer_progress, favs_list, false);
            //favs_list.addFooterView(searchFooter);
            searchMode = true;

            //favs_list.addHeaderView(createHeaderListView(query));

            //use the query to search your data somehow
            AsyncTask<String, Void, ArrayList<AudioItem>> searching = new AsyncTask<String,
                    Void, ArrayList<AudioItem>>() {

                @Override
                protected void onPreExecute() {
                    inLoads = audioHelper.searchInLoads(query);
                    adapter = new PlayListAdapter(inLoads, context);
                    favs_list.setAdapter(adapter);
                    searchOffset = 1;

                    int dividerPos = inLoads.size();
                    beginSongPos = dividerPos;
                    adapter.dividerPos = dividerPos;

                    if (inLoads.size() == 0) {
                        favs_list.setVisibility(View.GONE);
                        progressBar.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                protected ArrayList<AudioItem> doInBackground(String... param) {
                    String searchUrl = vkApi.getStringSearchRequest(param[0], 0);
                    return extractDataFromRequest(searchUrl, param[0]);
                }

                @Override
                protected void onPostExecute(ArrayList<AudioItem> result) {

                    for (int i = 0; i < result.size(); i++) {
                        inLoads.add(result.get(i));
                    }

                    adapter.notifyDataSetChanged();

                    adapter.playlist = inLoads;

                    progressBar.setVisibility(View.GONE);
                    favs_list.setVisibility(View.VISIBLE);

                }
            };
           /* if(searchingArt!=null) {
                searchingArt.execute(query);
            }*/
            searching.execute(query);
        }
    }

    ArrayList<AudioItem> extractDataFromRequest(String request, String user_query) {
        String getAudioRequest = vkApi.getAudioByIdsRequest(vkApi.getIdsBySearch(request));
        String lastFmArt;
        ArrayList<AudioItem> result = getAudioRequest == null ? new ArrayList<AudioItem>() :
                vkApi.getAudioByIds(getAudioRequest);

        if (isUA) {  // check for pics *only for ukraine
            LastFmApi lastFmApi = new LastFmApi();
            lastFmArt = lastFmApi.getArtUrl(lastFmApi.getArtUrlRequest(user_query));

            if (lastFmArt != null) {
                for (int i = 0; i < result.size(); i++) {
                    result.get(i).picUrl = lastFmArt;
                }
            }
        }
        return result;
    }

    ArrayList<AudioItem> mixSearchItems(ArrayList<AudioItem> loads, ArrayList<AudioItem> searchResult) {
        for (int i = 0; i < searchResult.size(); i++) {
            loads.add(searchResult.get(i));
        }
        return loads;
    }

    ViewGroup createHeaderListView(String query) {
        headerList = (ViewGroup) getLayoutInflater().inflate(R.layout.divider, favs_list, false);
       /* ListView list = (ListView) headerList.findViewById(R.id.playlist_header);
        list.setAdapter(new PlayListAdapter(audioHelper.loadMp3s(path), context));
        list.setOnClickListener(this);*/
        return headerList;
    }

    void removeHeaderListView() {
        favs_list.removeHeaderView(headerList);
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        progressBar.setVisibility(View.GONE);
        favs_list.setVisibility(View.VISIBLE);
        fab.setVisibility(View.VISIBLE);
        searchMode = false;
        query = null;
        //favs_list.removeFooterView(searchFooter);

        //removeHeaderListView();

        updateList();
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_make_playlist:
                createPlaylistPopup();
                break;
            /*case R.id.nav_all_songs:
                Intent playlistIntent = new Intent(this, PlaylistActivity.class);
                playlistIntent.putExtra("name", "all");
                startActivity(playlistIntent);
                break;*/
            case R.id.nav_settings:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingsIntent);
                break;
            case R.id.nav_share:
                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.share_app_title));
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, getResources().getString(R.string.share_app_text));
                startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.share_via)));
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
        AudioItem song = (AudioItem) parent.getItemAtPosition(position);
        String link = song.link;

        if (adapter.actionMode != null) {
            adapter.toggleSelection(position);
            adapter.updateActionModeTitle();
        } else if (song.isPlaylist) {
            Intent intent = new Intent(context, PlaylistActivity.class);
            intent.putExtra("name", song.name);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (link.equals(playInfo.activeLink) &&
                song.list_mark == playInfo.ACTIVE_LIST_MARK) {

            if (audioHelper.getIsPlaying()) {
                audioHelper.stopMp3();
            } else if (audioHelper.getIsSongConnected()) {
                audioHelper.playMp3();
            } else
                audioHelper.releaseMp3();
        } else {
            /*if (!CurrentPlayInfo.isNetworkConnected(context)) {
                Toast.makeText(context,
                        R.string.network_access_error_text, Toast.LENGTH_SHORT).show();
                return;
            }*/
            audioHelper.setFocusParams(position, view);
            audioHelper.setParent(parent);

            if (audioHelper != null) {
                ArrayList<AudioItem> list = ((PlayListAdapter) parent.getAdapter()).getActivePlaylist();

                audioHelper.releaseMp3();
                audioHelper.setActivePlaylist(list);
                audioHelper.setActivePlaylistIndex(list.indexOf(parent.getItemAtPosition(position)));
                audioHelper.setConnectionMp3((AudioItem) parent.getItemAtPosition(position), list, view);
            }
        }

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long id) {

        if (adapter.actionMode != null || searchMode) {
            return false;
        }

        favs_list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        favs_list.setItemChecked(pos, true);
        //fab.setVisibility(View.GONE);

        startActionMode(
                new ActionModeCallback(favs_list, this, playlistSize, fab, null, null, 0));
        adapter.toggleSelection(pos);

        adapter.actionMode.setTitle("1 selected");

        return true;
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int scrollState) {

        int btn_initPosY = fab.getScrollY();
        /*if (scrollState == SCROLL_STATE_TOUCH_SCROLL) {
            fab.animate().cancel();
            fab.animate().translationYBy(150);
        }*/
        if (scrollState == SCROLL_STATE_FLING) {
            if (goingUp) {
                fab.animate().cancel();
                fab.animate().translationY(btn_initPosY);
            } else {
                fab.animate().cancel();
                fab.animate().translationYBy(150);
            }
        }
        /*if() {
            fab.animate().cancel();
            fab.animate().translationY(btn_initPosY);
        }*/
    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        int btn_initPosY = fab.getScrollY();
        if (mLastFirstVisibleItem < firstVisibleItem) {
            goingUp = false;
        }
        if (mLastFirstVisibleItem > firstVisibleItem) {
            goingUp = true;
        }

        if (query != null && searchMode && !isLazyLoading && firstVisibleItem + visibleItemCount > totalItemCount - 1) {
            isLazyLoading = true;
            loadNext();
            //Toast.makeText(context, "last elem", Toast.LENGTH_LONG).show();
        }

        mLastFirstVisibleItem = firstVisibleItem;
    }


    void loadNext() {
        AsyncTask<String, Void, ArrayList<AudioItem>> searching = new AsyncTask<String,
                Void, ArrayList<AudioItem>>() {
            @Override
            protected void onPreExecute() {
            }

            @Override
            protected ArrayList<AudioItem> doInBackground(String... param) {
                String searchUrl = vkApi.getStringSearchRequest(query, searchOffset * 10);
                searchOffset++;
                return extractDataFromRequest(searchUrl, query);
            }

            @Override
            protected void onPostExecute(ArrayList<AudioItem> nextPortion) {
                for (int i = 0; i < nextPortion.size(); i++) {
                    inLoads.add(nextPortion.get(i));
                }
                adapter.playlist = inLoads;
                adapter.notifyDataSetChanged();
                isLazyLoading = false;
            }
        };
        searching.execute(query);

    }

    /*@Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }*/


}

