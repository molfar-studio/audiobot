package com.crossfeed.favdoc;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

import static com.crossfeed.favdoc.R.id.bottomPlayer;

/**
 * Created by hawk on 08.01.2017.
 */
public class FavsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener  {

    private View view;
    private ListView favs_list;
    private AudioHelper audioHelper;
    private TextView empty_favs;
    private AppCompatActivity activity;
    private RelativeLayout bottom_player;
    private SharedPreferences prefs;
    private SwipeRefreshLayout swipeRefreshLayout;
    private final int FAVS_LIST_MARK=1;
    PlayListAdapter adapter;
    ArrayList<AudioItem> result;
    Context context;
   // BottomPlayer bottomPlayer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //audioHelper = MainActivity.getAudioHelper();
        activity = (AppCompatActivity) getActivity();
        context = activity.getApplicationContext();
        result = new ArrayList<>();
        //audioHelper = MainActivity.getClickListener().getAudioHelper();
        //bottomPlayer = MainActivity.getBottomPlayer();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)  {
        if(view==null) {
            view = inflater.inflate(R.layout.favs_fragment, container, false);
            setReferences(view);
            initialize();
        }
        return  view;
    }

    public void setReferences(View view) {
        favs_list = (ListView) view.findViewById(R.id.playlist_favs);
        empty_favs = (TextView) view.findViewById(R.id.empty_favs);
        bottom_player = (RelativeLayout) activity.findViewById(bottomPlayer);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.favs_refresh_layout);
    }


    public void initialize() {

        prefs = context.getSharedPreferences("favs", 0);
        onStreamPlaylistClickListener clickListener = MainActivity.getClickListener();

        if(clickListener!=null)
            favs_list.setOnItemClickListener(clickListener);
        favs_list.setEmptyView(empty_favs);
        adapter = new PlayListAdapter(getFavsList(), context);
        favs_list.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.CircleStartColor);
    }


    public ArrayList<AudioItem> getFavsList() {
        ArrayList<AudioItem> items = new ArrayList<>();
        AudioItem item;

        Map<String, ?> prefs_values = prefs.getAll();
        if(prefs_values!=null)
        for(Map.Entry<String, ?> entry: prefs_values.entrySet()) {
            if(!"".equals(entry.getValue())) {
                item = new AudioItem();
                /*item.setLink(entry.getKey());
                item.setMp3Name(entry.getValue().toString());
                item.setListMark(FAVS_LIST_MARK);
                items.add(item);*/
            }
        }
        //bottomPlayer.setFavsListLength(items.size());
        return items;
    }

    @Override
    public void onRefresh() {
        adapter = new PlayListAdapter(getFavsList(), context);
        favs_list.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);
    }
}
